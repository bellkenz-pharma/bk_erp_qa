package stepDefinitions.System_UI_Navigation;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import stepDefinitions.Test_Base;

public class ForgotPassword_Steps extends Test_Base {
	
    @And("^User input (.*) as email address$")
    public void user_input_as_email_address(String email) throws Throwable {
    	erpForgotPasswordPage.GetEmailResetTextbox().clear();
    	erpForgotPasswordPage.GetEmailResetTextbox().sendKeys(email);
    }
    
    @And("^User open a new tab in the browser$")
    public void user_open_a_new_tab_in_the_browser() throws Throwable {
    	String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL, Keys.ENTER);
    	driver.findElement(By.linkText("send me back")).sendKeys(selectLinkOpeninNewTab);
    	
    	ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(1));
    }
    
    @Given("^User access the Gmail site$")
    public void user_access_the_gmail_site() throws Throwable {
        genericTasks.navigateToGmailURL();
    }

    @And("^User click the \"([^\"]*)\" button in (.*)$")
    public void user_click_the_something_button_in_something(String button, String module) throws Throwable {
        if (module.equals("gmail")) {
        	if (button.equals("Next")) {
            	gmailPage.GetNextButton().click();
            }
        } else if (module.equals("Add User")) {
        	if (button.equals("Add")) {
        		erpUserListPage.GetAddButton().click();
        	}
        } else if (module.equals("Edit User")) {
        	if (button.equals("Save Changes")) {
        		erpUserListPage.GetSaveChangesButton().click();
        	}
        }
    	
    }

    @And("^User inputs (.*) as (.*) address$")
    public void user_inputs_something_as_something_address(String emailAdd, String email) throws Throwable {
    	Thread.sleep(1500);
    	gmailPage.GetGmailTextbox().clear();
        gmailPage.GetGmailTextbox().sendKeys(emailAdd);
    }
    
    @And("^User inputs (.*) as (.*) password$")
    public void user_inputs_something_as_something_password(String password, String email) throws Throwable {
    	gmailPage.GetGmailPasswordTextbox().clear();
        gmailPage.GetGmailPasswordTextbox().sendKeys(password);
    }

    @And("^User click the \"([^\"]*)\" option in (.*)$")
    public void user_click_the_something_option_in_something(String option, String email) throws Throwable {
        if (option.equals("More")) {
        	gmailPage.GetMoreOption().click();
        } else if (option.equals("Spam")) {
        	gmailPage.GetSpamOption().click();
        }
    }
    
    @And("^User open the email for reset password$")
    public void user_open_the_email_for_reset_password() throws Throwable {
    	WebDriverWait wait = new WebDriverWait(driver, 10);
    	WebElement emailTable = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//table[@id=':md']")));
    	
    	List<WebElement> rowsEmailTR = emailTable.findElements(By.tagName("tr"));
    	for (int x = 0; x < rowsEmailTR.size(); x++ ) {
    		if (x == 0) {
    			List<WebElement> colsEmailTD = rowsEmailTR.get(x).findElements(By.tagName("td"));
    			for (int y = 0; y < colsEmailTD.size(); y++) {
    				if (y == 4) {
    					colsEmailTD.get(y).click();
    				}
        		}
    		}
    	}
    }
    
    @Then("^User access the reset password page$")
    public void user_access_the_reset_password_page() throws Throwable {		
		ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs2.get(2));
    }
    
    @And("^User change the password to (.*) password$")
    public void user_change_the_password_to_something_password(String password) throws Throwable {
        erpForgotPasswordPage.GetNewPasswordTextBox().sendKeys(password);
        erpForgotPasswordPage.GetConfirmPasswordTextBox().sendKeys(password);
    }
    
    @Then("^An invalid email error message is displayed$")
    public void an_invalid_email_error_message_is_displayed() throws Throwable {
        Assert.assertTrue(erpForgotPasswordPage.GetInvalidEmailDialogBox().isDisplayed());
    }
    
    @Then("^No email sent dialog box displayed$")
    public void no_email_sent_dialog_box_displayed() throws Throwable {
        Assert.assertFalse(erpForgotPasswordPage.GetEmailSentDialogBox().isDisplayed());
    }
    
}
