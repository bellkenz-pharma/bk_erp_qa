package stepDefinitions.System_UI_Navigation;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;

import cucumber.api.PendingException;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import stepDefinitions.Test_Base;

public class Login_Steps extends Test_Base {
	
	@Before
	public void beforeScenario() {
		System.out.println("[DEBUG] Setting up webdriver");
		SetUp();
	}
	
	@After
	public void afterScenario() {
//		KillDriver();
	}
	
	@Given("^User login as \"([^\"]*)\" account$")
    public void user_login_as_something_account(String accountType) throws Throwable {
        if (accountType.equals("Tope")) {
        	genericTasks.loginToERPQATope();
        } else if (accountType.equals("admin")) {
        	genericTasks.loginToERPAdmin();
        }
    }
	
	@Given("^User access the ERP QA Server Login page$")
    public void user_access_the_erp_qa_server_login_page() throws Throwable {
        genericTasks.navigateToERPURL();
    }
	
	@And("^User is logged in as \"([^\"]*)\"$")
    public void user_is_logged_in_as_something(String accountType) throws Throwable {
        
    }

    @When("^User click the \"([^\"]*)\" button$")
    public void user_click_the_something_button(String button) throws Throwable {
        if (button.equals("Sign in")) {
        	erpLoginPage.GetSignInButton().click();
        } else if (button.equals("Logout")) {
        	erpHomePage.GetLogoutButton().click();
        } else if (button.equals("Send me new password")) {
        	erpForgotPasswordPage.GetSendMeButton().click();
        	Assert.assertTrue(erpForgotPasswordPage.GetEmailSentDialogBox().isDisplayed());
        } else if (button.equals("Update Password")) {
        	erpForgotPasswordPage.GetUpdatePasswordButton().click();
        	Assert.assertTrue(erpForgotPasswordPage.GetPasswordChangedDialogBox().isDisplayed());
        	erpForgotPasswordPage.GetPasswordChangedOKButton().click();
        } else if (button.equals("Add User")) {
        	erpUserListPage.GetAddUserButton().click();
        }
    }

    @Then("^User successfully accessed the ERP Home Page$")
    public void user_successfully_accessed_the_erp_home_page() throws Throwable {
    	Assert.assertTrue(erpHomePage.GetBKLogo().isDisplayed());
    }

    @And("^User input (.*) as username$")
    public void user_input_something_as_username(String username) throws Throwable {
    	erpLoginPage.GetUsernameTextbox().clear();
    	erpLoginPage.GetUsernameTextbox().sendKeys(username);
    }

    @And("^User input (.*) as password$")
    public void user_input_something_as_password(String password) throws Throwable {
        erpLoginPage.GetPasswordTextbox().clear();
        erpLoginPage.GetPasswordTextbox().sendKeys(password);
    }
    
    @Then("^An \"([^\"]*)\" error message displayed$")
    public void an_something_error_message_displayed(String errMsg) throws Throwable {
    	if (errMsg.equals("Invalid Credentials")) {
    		Assert.assertEquals(erpLoginPage.GetErrorLoginErrorMessage().getText(), errMsg);
    	}
    }
    
    @And("^User click the \"([^\"]*)\" link$")
    public void user_click_the_something_link(String link) throws Throwable {
        if (link.equals("User Menu")) {
        	erpHomePage.GetUserMenu().click();
        } else if (link.equals("Forgot Password")) {
        	erpLoginPage.GetForgotPasswordLink().click();
        } else if (link.equals("Reset Password")) {
        	gmailPage.GetResetLinkFromEmail().click();
        	String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.ENTER);
        	driver.findElement(By.linkText("Reset Password")).sendKeys(selectLinkOpeninNewTab);
        }
    }
    
    @Then("^User successfully logged out from BK ERP$")
    public void user_successfully_logged_out_from_bk_erp() throws Throwable {
        Assert.assertTrue(erpLoginPage.GetSignInButton().isDisplayed());
    }
    
    
    
    
}
