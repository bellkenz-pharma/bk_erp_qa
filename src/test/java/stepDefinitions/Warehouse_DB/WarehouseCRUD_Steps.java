package stepDefinitions.Warehouse_DB;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import stepDefinitions.Test_Base;

public class WarehouseCRUD_Steps extends Test_Base {
	
	@And("^User input the \"([^\"]*)\" warehouse test data$")
    public void user_input_the_something_warehouse_test_data(String crudWarehouse) throws Throwable {
        
        File desktop = new File(System.getProperty("user.home"), "Desktop");
		FileInputStream inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Warehouse_DB" + File.separator + "Create_Warehouse.xlsx");
	    XSSFWorkbook wB = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wB.getSheetAt(0);
		
		int colCount = sheet.getRow(0).getLastCellNum();
		int rowCount = sheet.getLastRowNum();
		
		System.out.println("colCount: " + colCount);
		System.out.println("rowCount: " + rowCount);
		
		for (int x = 1; x <= rowCount; x++) {
			erpWarehouseListPage.GetCreateNewWarehouseButton().click();
			for (int y = 0; y < colCount; y++) {
				XSSFCell cell = sheet.getRow(x).getCell(y);
				if (y == 0) {
					erpWarehouseCRUDPage.GetWarehouseNameTextbox().clear();
					erpWarehouseCRUDPage.GetWarehouseNameTextbox().sendKeys(cell.getStringCellValue());
				} else if (y == 1) {
					erpWarehouseCRUDPage.GetBuildingTextbox().clear();
					erpWarehouseCRUDPage.GetBuildingTextbox().sendKeys(cell.getStringCellValue());
				} else if (y == 2) {
					erpWarehouseCRUDPage.GetLotStreetTextbox().clear();
					erpWarehouseCRUDPage.GetLotStreetTextbox().sendKeys(cell.getStringCellValue());
				} else if (y == 3) {
					List<String> floorsList = Arrays.asList(cell.getStringCellValue().split(","));
					System.out.println("floorsList.size(): " + floorsList.size());
					for (int z = 0; z <= floorsList.size(); z++) {
						
					}
//					erpWarehouseCRUDPage.GetFloorTextbox(x).clear();
					
				}
			}
		}
    }
	
	@Then("^The warehouse \"([^\"]*)\" is successfully created$")
    public void the_warehouse_something_is_successfully_created(String crudWarehouse) throws Throwable {
        throw new PendingException();
    }

}
