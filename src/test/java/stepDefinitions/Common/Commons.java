package stepDefinitions.Common;

import cucumber.api.java.en.And;
import stepDefinitions.Test_Base;

public class Commons extends Test_Base {

	@And("^User click the \"([^\"]*)\" menu$")
    public void user_click_the_something_menu(String menu) throws Throwable {
        if (menu.equals("User Control")) {
        	erpUserListPage.GetUserControlMenu().click();
        } else if (menu.equals("Master Databases")) {
        	erpHomePage.GetMasterDatabasesMenu().click();
        } else if (menu.equals("Product Databases")) {
        	erpHomePage.GetProductDatabasesSubMenu().click();
        } else if (menu.equals("Product List")) {
        	erpHomePage.GetProductListSubMenu().click();
        } else if (menu.equals("User Database")) {
        	erpHomePage.GetUserDatabaseSubMenu().click();
        } else if (menu.equals("Users Control")) {
        	erpHomePage.GetUsersControlSubMenu().click();
        } else if (menu.equals("Customer Databases")) {
        	erpHomePage.GetCustomerDatabasesSubMenu().click();
        } else if (menu.equals("Customer List")) {
        	erpHomePage.GetCustomerListSubMenu().click();
        } else if (menu.equals("Product Databases")) {
        	erpHomePage.GetProductDatabasesSubMenu().click();
        } else if (menu.equals("Product List")) {
        	erpHomePage.GetProductListSubMenu().click();
        } else if (menu.equals("Warehouse Database")) {
        	erpHomePage.GetWarehouseDatabaseSubMenu().click();
        } else if (menu.equals("Warehouse List")) {
        	erpHomePage.GetWarehouseListSubMenu().click();
        }
    }

}
