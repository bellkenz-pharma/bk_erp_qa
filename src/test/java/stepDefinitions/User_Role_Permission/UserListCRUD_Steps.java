package stepDefinitions.User_Role_Permission;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import stepDefinitions.Test_Base;

public class UserListCRUD_Steps extends Test_Base {
	
	@Then("^User input (.*) as \"([^\"]*)\" in Add User$")
    public void user_input_something_as_something_in_add_user(String inputValue, String field) throws Throwable {
        if (field.equals("username")) {
        	erpUserListPage.GetUsernameTextbox().clear();
        	erpUserListPage.GetUsernameTextbox().sendKeys(inputValue);
        } else if (field.equals("employee number")) {
        	erpUserListPage.GetEmployeeNoTextbox().clear();
        	erpUserListPage.GetEmployeeNoTextbox().sendKeys(inputValue);
        } else if (field.equals("last name")) {
        	erpUserListPage.GetLastNameTextbox().clear();
        	erpUserListPage.GetLastNameTextbox().sendKeys(inputValue);
        } else if (field.equals("first name")) {
        	erpUserListPage.GetFirstNameTextbox().clear();
        	erpUserListPage.GetFirstNameTextbox().sendKeys(inputValue);
        } else if (field.equals("email")) {
        	erpUserListPage.GetEmailTextbox().clear();
        	erpUserListPage.GetEmailTextbox().sendKeys(inputValue);
        } else if (field.equals("password")) {
        	erpUserListPage.GetPasswordTextbox().sendKeys(inputValue);
        } else if (field.equals("department")) {
        	Select departmentDropdown = new Select(erpUserListPage.GetDepartmentDropdown());
        	departmentDropdown.selectByVisibleText(inputValue);
        } else if (field.equals("job")) {
        	Select jobDropdown = new Select(erpUserListPage.GetJobDropdown());
        	jobDropdown.selectByVisibleText(inputValue);
        } else if (field.equals("contact number")) {
        	erpUserListPage.GetContactNoTextbox().clear();
        	erpUserListPage.GetContactNoTextbox().sendKeys(inputValue);
        }
    }
	
	@Then("^The employee number (.*) is now added successfully$")
    public void the_employee_number_something_is_now_added_successfully(String empNo) throws Throwable {
		String dataValue = "";
//		String dataID = "";
		
		Thread.sleep(2000);
		erpUserListPage.GetSearchTextbox().sendKeys(empNo);
		erpUserListPage.GetSearchTextbox().sendKeys(Keys.ENTER);
		
		List<WebElement> elements = erpUserListPage.GetUserListTable().findElements(By.cssSelector("div"));
		for (int x = 0; x < elements.size(); x++) {
			dataValue = elements.get(x).getAttribute("data-value");
//			System.out.println("Table Value: " + "'" + dataValue + "'");
			
			if (x == 3) {
				
				System.out.println(dataValue);
				System.out.println(empNo);
				
				Assert.assertEquals(dataValue, empNo);
				
			}
			
//			try {
//				if (dataValue.equals(empNo)) {
//					dataID = elements.get(x - 2).getAttribute("data-id");
//					System.out.println("Table Value: " + dataID);
//					List<WebElement> tableRow = erpUserListPage.GetUserListRowTable(dataID).findElements(By.cssSelector("div"));
//					for (int y = 0; y < tableRow.size(); y++) {
//						String tableValue = tableRow.get(y).getText();
//						System.out.println("Table Value: " + tableValue);
//					}
//				}
//			} catch (Exception e) {
//				System.out.println("CATCH!");
//			}
			
		}
    }
	
	@When("^User click the \"([^\"]*)\" icon of (.*) user$")
    public void user_click_the_something_icon_of_something_user(String icon, String empNo) throws Throwable {
		String dataID = "";
		
		Thread.sleep(2000);
		erpUserListPage.GetSearchTextbox().sendKeys(empNo);
		erpUserListPage.GetSearchTextbox().sendKeys(Keys.ENTER);
		
		List<WebElement> elements = erpUserListPage.GetUserListTable().findElements(By.cssSelector("div"));
		for (int x = 0; x < elements.size(); x++) {
			if (x == 0) {
				dataID = elements.get(x).getAttribute("data-ID");	
			}
			if (x == 11) {
				if (icon.equals("Edit")) {
					erpUserListPage.GetEditIconPerUser(dataID).click();
				} else if (icon.equals("Change Password")) {
					erpUserListPage.GetChangePasswordIconPerUser(dataID).click();
				} else if (icon.equals("Edit Role")) {
					erpUserListPage.GetEditRoleIconPerUser(dataID).click();
				}
			}
			
		}
    }
	
	@When("^User input (.*) as \"([^\"]*)\" in Edit User$")
    public void user_input_as_something_in_edit_user(String inputValue, String field) throws Throwable {
		if (field.equals("username")) {
        	erpUserListPage.GetUsernameTextbox().clear();
        	erpUserListPage.GetUsernameTextbox().sendKeys(inputValue);
        } else if (field.equals("employee number")) {
        	erpUserListPage.GetEmployeeNoTextbox().clear();
        	erpUserListPage.GetEmployeeNoTextbox().sendKeys(inputValue);
        } else if (field.equals("last name")) {
        	erpUserListPage.GetLastNameTextbox().clear();
        	erpUserListPage.GetLastNameTextbox().sendKeys(inputValue);
        } else if (field.equals("first name")) {
        	erpUserListPage.GetFirstNameTextbox().clear();
        	erpUserListPage.GetFirstNameTextbox().sendKeys(inputValue);
        } else if (field.equals("email")) {
        	erpUserListPage.GetEmailTextbox().clear();
        	erpUserListPage.GetEmailTextbox().sendKeys(inputValue);
        } else if (field.equals("password")) {
        	erpUserListPage.GetPasswordTextbox().sendKeys(inputValue);
        } else if (field.equals("department")) {
        	Select departmentDropdown = new Select(erpUserListPage.GetDepartmentDropdown());
        	departmentDropdown.selectByVisibleText(inputValue);
        } else if (field.equals("job")) {
        	Select jobDropdown = new Select(erpUserListPage.GetJobDropdown());
        	jobDropdown.selectByVisibleText(inputValue);
        } else if (field.equals("contact number")) {
        	erpUserListPage.GetContactNoTextbox().clear();
        	erpUserListPage.GetContactNoTextbox().sendKeys(inputValue);
        }
    }
	
	@Then("^User input the (.*) as \"([^\"]*)\" password$")
    public void user_input_the_as_something_password(String password, String typeOfPass) throws Throwable {
        if (typeOfPass.equals("current")) {
        	erpUserListPage.GetCurrentPasswordTextbox().clear();
        	erpUserListPage.GetCurrentPasswordTextbox().sendKeys(password);
        } else if (typeOfPass.equals("new")) {
        	erpUserListPage.GetNewPasswordTextbox().clear();
        	erpUserListPage.GetNewPasswordTextbox().sendKeys(password);
        }
    }
    
    @Then("^User login the (.*) of (.*) with (.*) username$")
    public void user_login_the_of_with_username(String newPass, String employeeNo, String username) throws Throwable {
        erpLoginPage.GetUsernameTextbox().clear();
        erpLoginPage.GetUsernameTextbox().sendKeys(username);
        erpLoginPage.GetPasswordTextbox().clear();
        erpLoginPage.GetPasswordTextbox().sendKeys(newPass);
        erpLoginPage.GetSignInButton().click();
    }
	
	@And("^User successfully login using the new password$")
    public void user_successfully_login_using_the_new_password() throws Throwable {
		Assert.assertTrue(erpHomePage.GetBKLogo().isDisplayed());
    }
	
	@Then("^A duplicate \"([^\"]*)\" error message displayed$")
    public void a_duplicate_something_error_message_displayed(String errMsg) throws Throwable {
        if (errMsg.equals("Username")) {
        	Assert.assertEquals("The username has already been taken.", erpUserListPage.GetUsernameDupErrorMsg().getText());
        } else if (errMsg.equals("Employee No")) {
        	Assert.assertEquals("The employee no has already been taken.", erpUserListPage.GetUsernameDupErrorMsg().getText());
        } else if (errMsg.equals("Email")) {
        	Assert.assertEquals("The email has already been taken.", erpUserListPage.GetUsernameDupErrorMsg().getText());
        }
    }


}
