package stepDefinitions.User_Role_Permission;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import stepDefinitions.Test_Base;

public class UserList_Steps extends Test_Base {
	
//	@And("^User click the \"([^\"]*)\" menu$")
//    public void user_click_the_something_menu(String menu) throws Throwable {
//        if (menu.equals("User Control")) {
//        	erpUserListPage.GetUserControlMenu().click();
//        } else if (menu.equals("Master Databases")) {
//        	erpHomePage.GetMasterDatabasesMenu().click();
//        } else if (menu.equals("Product Databases")) {
//        	erpHomePage.GetProductDatabasesSubMenu().click();
//        } else if (menu.equals("Product List")) {
//        	erpHomePage.GetProductListSubMenu().click();
//        } else if (menu.equals("User Database")) {
//        	erpHomePage.GetUserDatabaseSubMenu().click();
//        } else if (menu.equals("Users Control")) {
//        	erpHomePage.GetUsersControlSubMenu().click();
//        }
//    }

    @And("^User click \"([^\"]*)\" tab$")
    public void user_click_something_tab(String tab) throws Throwable {
        erpUserListPage.GetUserManagementMenu().click();
    }
    
    @Then("^\"([^\"]*)\" button is displayed$")
    public void something_button_is_displayed(String button) throws Throwable {
    	if (button.equals("Add User")) {
    		Assert.assertTrue(erpUserListPage.GetAddUserButton().isDisplayed());
    	}
    }
    
    @And("^\"([^\"]*)\" textbox is displayed$")
    public void something_textbox_is_displayed(String textbox) throws Throwable {
        if (textbox.equals("Search User List")) {
        	Assert.assertTrue(erpUserListPage.GetSearchTextbox().isDisplayed());
        }
    }
    
    @And("^\"([^\"]*)\" table is displayed$")
    public void something_table_is_displayed(String table) throws Throwable {
        if (table.equals("User List")) {
        	Assert.assertTrue(erpUserListPage.GetUserListTable().isDisplayed());
        }
    }
    
    @And("^User List table has \"([^\"]*)\" table header$")
    public void user_list_table_has_something_table_header(String tableHeader) throws Throwable {
    	if (tableHeader.equals("Active")) {
        	Assert.assertTrue(erpUserListPage.GetActiveHeader().isDisplayed());
        } else if (tableHeader.equals("Employee No.")) {
        	Assert.assertTrue(erpUserListPage.GetEmployeeNoHeader().isDisplayed());
        } else if (tableHeader.equals("First Name")) {
        	Assert.assertTrue(erpUserListPage.GetFirstNameHeader().isDisplayed());
        } else if (tableHeader.equals("Last Name")) {
        	Assert.assertTrue(erpUserListPage.GetLastNameHeader().isDisplayed());
        } else if (tableHeader.equals("Email")) {
        	Assert.assertTrue(erpUserListPage.GetEmailHeader().isDisplayed());
        } else if (tableHeader.equals("Username")) {
        	Assert.assertTrue(erpUserListPage.GetUsernameHeader().isDisplayed());
        } else if (tableHeader.equals("Department")) {
        	Assert.assertTrue(erpUserListPage.GetDepartmentHeader().isDisplayed());
        } else if (tableHeader.equals("Job Title")) {
        	Assert.assertTrue(erpUserListPage.GetJobTitleHeader().isDisplayed());
        } else if (tableHeader.equals("Roles")) {
        	Assert.assertTrue(erpUserListPage.GetRolesHeader().isDisplayed());
        } else if (tableHeader.equals("Action")) {
        	Assert.assertTrue(erpUserListPage.GetActionHeader().isDisplayed());
        }
    }
    
    @Then("^Add User has \"([^\"]*)\" textbox$")
    public void add_user_has_something_textbox(String textbox) throws Throwable {
        if (textbox.equals("Username")) {
        	Assert.assertTrue(erpUserListPage.GetUsernameTextbox().isDisplayed());
        } else if (textbox.equals("Employee Number")) {
        	Assert.assertTrue(erpUserListPage.GetEmployeeNoTextbox().isDisplayed());
        } else if (textbox.equals("Last Name")) {
        	Assert.assertTrue(erpUserListPage.GetLastNameTextbox().isDisplayed());
        } else if (textbox.equals("First Name")) {
        	Assert.assertTrue(erpUserListPage.GetFirstNameTextbox().isDisplayed());
        } else if (textbox.equals("Email")) {
        	Assert.assertTrue(erpUserListPage.GetEmailTextbox().isDisplayed());
        } else if (textbox.equals("Password")) {
        	Assert.assertTrue(erpUserListPage.GetDepartmentDropdown().isDisplayed());
        } else if (textbox.equals("Job")) {
        	Assert.assertTrue(erpUserListPage.GetJobDropdown().isDisplayed());
        } else if (textbox.equals("Contact Number")) {
        	Assert.assertTrue(erpUserListPage.GetContactNoTextbox().isDisplayed());
        }
    }
    
    @Then("^Edit User has \"([^\"]*)\" textbox$")
    public void edit_user_has_something_textbox(String textbox) throws Throwable {
    	if (textbox.equals("Username")) {
        	Assert.assertTrue(erpUserListPage.GetUsernameTextbox().isDisplayed());
        } else if (textbox.equals("Employee Number")) {
        	Assert.assertTrue(erpUserListPage.GetEmployeeNoTextbox().isDisplayed());
        } else if (textbox.equals("Last Name")) {
        	Assert.assertTrue(erpUserListPage.GetLastNameTextbox().isDisplayed());
        } else if (textbox.equals("First Name")) {
        	Assert.assertTrue(erpUserListPage.GetFirstNameTextbox().isDisplayed());
        } else if (textbox.equals("Email")) {
        	Assert.assertTrue(erpUserListPage.GetEmailTextbox().isDisplayed());
        } else if (textbox.equals("Password")) {
        	Assert.assertTrue(erpUserListPage.GetDepartmentDropdown().isDisplayed());
        } else if (textbox.equals("Job")) {
        	Assert.assertTrue(erpUserListPage.GetJobDropdown().isDisplayed());
        } else if (textbox.equals("Contact Number")) {
        	Assert.assertTrue(erpUserListPage.GetContactNoTextbox().isDisplayed());
        }
    }
    
    @When("^User click the \"([^\"]*)\" icon$")
    public void user_click_the_something_icon(String icon) throws Throwable {
        if (icon.equals("Edit")) {
        	erpUserListPage.Get1stEditIcon().click();
        } else if (icon.equals("Change Password")) {
        	
        }
    }
    
    @Then("^Change Password has \"([^\"]*)\" textbox$")
    public void change_password_has_something_textbox(String textbox) throws Throwable {
        if (textbox.equals("Current Password")) {
        	Assert.assertTrue(erpUserListPage.GetCurrentPasswordTextbox().isDisplayed());
        } else if (textbox.equals("New Password")) {
        	Assert.assertTrue(erpUserListPage.GetNewPasswordTextbox().isDisplayed());
        } else if (textbox.equals("Confirm Password")) {
        	Assert.assertTrue(erpUserListPage.GetConfirmPasswordTextbox().isDisplayed());
        }
    }
}


//String dataValue = "";
//String dataID = "";
//System.out.println();
//List<WebElement> elements = erpUserListPage.GetUserListTable().findElements(By.cssSelector("div"));
//for (int x = 0; x < elements.size(); x++) {
//	dataValue = elements.get(x).getAttribute("data-value");
//	try {
//		if (dataValue.equals("TEMP-001")) {
//			dataID = elements.get(x - 2).getAttribute("data-id");
//			List<WebElement> tableRow = erpUserListPage.GetUserListRowTable(dataID).findElements(By.cssSelector("div"));
//			for (int y = 0; y < tableRow.size(); y++) {
//				String tableValue = tableRow.get(y).getText();
//				System.out.println("Table Value: " + tableValue);
//			}
//		}
//	} catch (Exception e) {
//		
//	}
//	
//}