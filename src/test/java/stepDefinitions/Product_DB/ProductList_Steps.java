package stepDefinitions.Product_DB;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import stepDefinitions.Test_Base;

public class ProductList_Steps extends Test_Base {
	
	@When("^User successfully accessed the \"([^\"]*)\" page$")
    public void user_successfully_accessed_the_something_page(String webPage) throws Throwable {
        if (webPage.equals("Product List")) {
        	Assert.assertTrue(erpProductListPage.GetProductListHeader().isDisplayed());
        } else if (webPage.equals("Warehouse List")) {
        	Assert.assertTrue(erpWarehouseListPage.GetWarehouseHeader().isDisplayed());
        }
    }
	
	@Then("^Table header name is \"([^\"]*)\" for Product List table$")
    public void table_header_name_is_something_for_product_list_table(String tableHeader) throws Throwable {
		Assert.assertEquals(tableHeader, erpProductListPage.GetProductListTableHeader().getText());
    }
	
	@And("^The \"([^\"]*)\" button is displayed in Product List page$")
    public void the_something_button_is_displayed_in_product_list_page(String button) throws Throwable {
    	if (button.equals("Create New Product")) {
    		erpProductListPage.GetCreateNewProductButton().isDisplayed();
    	}
    }
	
	@And("^The \"([^\"]*)\" table column is displayed in Product List page$")
    public void the_something_table_column_is_displayed_in_product_list_page(String tableColumn) throws Throwable {
		if (tableColumn.equals("Product Code")) {
			Assert.assertTrue(erpProductListPage.GetProductCodeColumn().isDisplayed());
		} else if (tableColumn.equals("Product Name")) {
			Assert.assertTrue(erpProductListPage.GetProductNameColumn().isDisplayed());
		} else if (tableColumn.equals("Generic Name")) {
			Assert.assertTrue(erpProductListPage.GetGenericNameColumn().isDisplayed());
		} else if (tableColumn.equals("Supplier")) {
			Assert.assertTrue(erpProductListPage.GetSupplierColumn().isDisplayed());
		} else if (tableColumn.equals("Product Segment")) {
			Assert.assertTrue(erpProductListPage.GetProductSegmentColumn().isDisplayed());
		} else if (tableColumn.equals("Product Group")) {
			Assert.assertTrue(erpProductListPage.GetProductGroupColumn().isDisplayed());
		} else if (tableColumn.equals("Packaging")) {
			Assert.assertTrue(erpProductListPage.GetPackagingColumn().isDisplayed());
		} else if (tableColumn.equals("Qty Per Pack")) {
			Assert.assertTrue(erpProductListPage.GetQtyPerPackColumn().isDisplayed());
		} else if (tableColumn.equals("Stock Level")) {
			Assert.assertTrue(erpProductListPage.GetStockLevelColumn().isDisplayed());
		} else if (tableColumn.equals("CPR No.")) {
			Assert.assertTrue(erpProductListPage.GetCPRNoColumn().isDisplayed());
		} else if (tableColumn.equals("CPR Expiry")) {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
			
			EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
			eventFiringWebDriver.executeScript("document.querySelector('div[class=\"MuiDataGrid-window\"]').scrollLeft=500");
			
			Assert.assertTrue(erpProductListPage.GetCPRExpiryColumn().isDisplayed());
		} else if (tableColumn.equals("Active")) {
			Assert.assertTrue(erpProductListPage.GetActiveColumn().isDisplayed());
		} 
		
		// FOR LOOPING COLUMN
		//		String dataValue = "";
//		
//		List<WebElement> headerElements = erpProductListPage.GetProductListHeaderElement().findElements(By.cssSelector("div"));
//		for (int x = 1; x < headerElements.size(); x++) {
//			dataValue = headerElements.get(x).getText();
//			if (x == 0) {
//				List<String> columns = Arrays.asList(dataValue.split("\n"));
//				System.out.println("columns: " + columns);
//				for (int y = 0; y < columns.size(); y++) {
//					if (tableColumn.equals("Product Code")) {
//						Assert.assertEquals(tableColumn, columns.get(0));
//					} else if (tableColumn.equals("Product Code")) {
//						Assert.assertEquals(tableColumn, columns.get(0));
//					}
//				}
//			}
//		}
    }
	
	@Then("^The product named \"([^\"]*)\" is successfully created and displayed in Product List$")
    public void the_product_named_something_is_successfully_created_and_displayed_in_product_list(String prodName) throws Throwable {
        if (erpProductListPage.GetProductListTableHeader().isDisplayed()) {
        	String dataValue = "";
        	
//        	JavascriptExecutor js = (JavascriptExecutor) driver;
//			EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
        	
        	erpProductListPage.GetSearchBox().sendKeys(prodName);
        	List<WebElement> prodListDataTable = erpProductListPage.GetProductListDataTable().findElements(By.cssSelector("div"));
        	for (int x = 0; x < prodListDataTable.size(); x++) {
//        		eventFiringWebDriver.executeScript("document.querySelector('div[class=\"MuiDataGrid-window\"]').scrollRight=500");
        		dataValue = prodListDataTable.get(x).getText();
        		
        		if (x == 2) {
        			Assert.assertEquals(prodName, dataValue);
        		}
        		
//        		if (x == 6) {
//        			js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
//        			eventFiringWebDriver.executeScript("document.querySelector('div[class=\"MuiDataGrid-window\"]').scrollLeft=500");
//        		}
        	}
        }
        
    }
	
	@And("^User click \"([^\"]*)\" product name in Product List Page$")
    public void user_click_something_product_name_in_product_list_page(String prodNameLink) throws Throwable {
		if (erpProductListPage.GetProductListTableHeader().isDisplayed()) {
			Thread.sleep(1000);
			erpProductListPage.GetSearchBox().sendKeys(prodNameLink);
			List<WebElement> prodListDataTable = erpProductListPage.GetProductListDataTable().findElements(By.cssSelector("div"));
	    	for (int x = 0; x < prodListDataTable.size(); x++) {  		
	    		if (x == 1) {
	    			String prodCode = prodListDataTable.get(x).getAttribute("data-value");
	    			erpProductListPage.GetProductCodeLink(prodCode).click();
	    		}
	    	}
		}
		
    }
	
}
