package stepDefinitions.Product_DB;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
//import org.apache.poi.ss.usermodel.Sheet;
//import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import stepDefinitions.Test_Base;

public class ProductCRUD_Steps extends Test_Base {
	
	@Then("^User click \"([^\"]*)\" button in Product List page$")
    public void user_click_something_button_in_product_list_page(String button) throws Throwable {
        if (button.equals("Create New Product")) {
        	erpProductListPage.GetCreateNewProductButton().click();
        }
    }
	
//	@And("^User input \"([^\"]*)\" as \"([^\"]*)\" in Product CRUD page$")
//    public void user_input_something_as_something_in_product_crud_page(String inputText, String textBox) throws Throwable {
//		if (textBox.equals("Product Name")) {
//			erpProductCRUDPage.GetProductNameTextbox().clear();
//			erpProductCRUDPage.GetProductNameTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Generic Name")) {
//			erpProductCRUDPage.GetGenericNameTextbox().clear();
//			erpProductCRUDPage.GetGenericNameTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Manufacturer")) {
//			erpProductCRUDPage.GetManufacturerTextbox().clear();
//			erpProductCRUDPage.GetManufacturerTextbox().sendKeys(inputText);
//		} else if (textBox.equals("CPR Number")) {
//			erpProductCRUDPage.GetCPRNoTextbox().clear();
//			erpProductCRUDPage.GetCPRNoTextbox().sendKeys(inputText);
//		} else if (textBox.equals("CPR Expiry")) {
//			erpProductCRUDPage.GetCPRExpiryTextbox().clear();
//			List<String> completeDate = Arrays.asList(inputText.split(" "));
//			erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(0));
//			erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(Keys.TAB);
//			erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(1));
//			erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(2));
//		} else if (textBox.equals("Stock Level")) {
//			erpProductCRUDPage.GetStockLevelTextbox().clear();
//			erpProductCRUDPage.GetStockLevelTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Shelf Life")) {
//			erpProductCRUDPage.GetShelfLifeTextbox().clear();
//			erpProductCRUDPage.GetShelfLifeTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Minimum Order Quantity")) {
//			erpProductCRUDPage.GetMinOrderQtyTextbox().clear();
//			erpProductCRUDPage.GetMinOrderQtyTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Safety Stock Level")) {
//			erpProductCRUDPage.GetSafetyStockLevelTextbox().clear();
//			erpProductCRUDPage.GetSafetyStockLevelTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Forecast")) {
//			erpProductCRUDPage.GetForecastTextbox().clear();
//			erpProductCRUDPage.GetForecastTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Supplier Lead Time")) {
//			erpProductCRUDPage.GetSupplierLeadTimeTextbox().clear();
//			erpProductCRUDPage.GetSupplierLeadTimeTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Cost Per Unit")) {
//			erpProductCRUDPage.GetCostPerUnitTextbox().clear();
//			erpProductCRUDPage.GetCostPerUnitTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Cost Per Box")) {
//			erpProductCRUDPage.GetCostPerBoxTextbox().clear();
//			erpProductCRUDPage.GetCostPerBoxTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Selling Price Per Box")) {
//			erpProductCRUDPage.GetSellingPricePerBoxTextbox().clear();
//			erpProductCRUDPage.GetSellingPricePerBoxTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Packaging Description")) {
//			erpProductCRUDPage.GetPackagingDescriptionTextbox().clear();
//			erpProductCRUDPage.GetPackagingDescriptionTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Qty Per Pack")) {
//			erpProductCRUDPage.GetQtyPerPackTextbox().clear();
//			erpProductCRUDPage.GetQtyPerPackTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Size Small")) {
//			erpProductCRUDPage.GetSizeSmallTextbox().clear();
//			erpProductCRUDPage.GetSizeSmallTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Size Big")) {
//			erpProductCRUDPage.GetSizeBigTextbox().clear();
//			erpProductCRUDPage.GetSizeBigTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Weight Net")) {
//			erpProductCRUDPage.GetWeightNetTextbox().clear();
//			erpProductCRUDPage.GetWeightNetTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Weight Gross")) {
//			erpProductCRUDPage.GetWeightGrossTextbox().clear();
//			erpProductCRUDPage.GetWeightGrossTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Temperature Min")) {
//			erpProductCRUDPage.GetTemperatureMinTextbox().clear();
//			erpProductCRUDPage.GetTemperatureMinTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Temperature Max")) {
//			erpProductCRUDPage.GetTemperatureMaxTextbox().clear();
//			erpProductCRUDPage.GetTemperatureMaxTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Humidity")) {
//			erpProductCRUDPage.GetHumidityTextbox().clear();
//			erpProductCRUDPage.GetHumidityTextbox().sendKeys(inputText);
//		} else if (textBox.equals("Storage Requirement")) {
//			erpProductCRUDPage.GetStorageRequirementTextbox().clear();
//			erpProductCRUDPage.GetStorageRequirementTextbox().sendKeys(inputText);
//		}
//    }
//	
//	@And("^User select \"([^\"]*)\" as \"([^\"]*)\" in Product CRUD page$")
//    public void user_select_something_as_something_in_product_crud_page(String option, String dropDown) throws Throwable {
//        if (dropDown.equals("Product Segment")) {
//        	Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
//        	prodSegDropDown.selectByVisibleText(option);
//        } else if (dropDown.equals("Product Sub-Segment")) {
//        	Select prodSubSegDropdown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
//        	prodSubSegDropdown.selectByVisibleText(option);
//        } else if (dropDown.equals("Product Group")) {
//        	Select prodGrpDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
//        	prodGrpDropDown.selectByVisibleText(option);
//        } else if (dropDown.equals("Product Sub-Group")) {
//        	Select prodSubGrpDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
//        	prodSubGrpDropDown.selectByVisibleText(option);
//        } else if (dropDown.equals("Supplier")) {
//        	Select supplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
//        	supplierDropDown.selectByVisibleText(option);
//        } else if (dropDown.equals("Packaging")) {
//        	Select packagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
//        	packagingDropDown.selectByVisibleText(option);
//        } else if (dropDown.equals("Unit of Measure")) {
//        	Select unitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
//        	unitOfMeasureDropDown.selectByVisibleText(option);
//        }
//    }
//	
//	@And("^User upload \"([^\"]*)\" as \"([^\"]*)\" in Product CRUD page$")
//    public void user_upload_something_as_something_in_product_crud_page(String fileType, String uploadField) throws Throwable {
//        if (uploadField.equals("CPR Document")) {
//        	erpProductCRUDPage.GetCPRDocumentUpload().sendKeys("");
//        } else if (uploadField.equals("Small Size Image")) {
//        	erpProductCRUDPage.GetSmallSizeImageUpload().sendKeys("");
//        } else if (uploadField.equals("Big Size Image")) {
//        	erpProductCRUDPage.GetBigSizeImageUpload().sendKeys("");
//        }
//    }
//	
	@And("^User click \"([^\"]*)\" tab in Product CRUD page$")
    public void user_click_something_tab_in_product_crud_page(String tab) throws Throwable {
        if (tab.equals("Packaging")) {
        	erpProductCRUDPage.GetPackagingTab().click();
        }
    }
//	
	@When("^User click \"([^\"]*)\" button in Product CRUD page$")
    public void user_click_something_button_in_product_crud_page(String button) throws Throwable {
        if (button.equals("Submit")) {
        	erpProductCRUDPage.GetSubmitButton().click();
        } else if (button.equals("Edit")) {
        	Thread.sleep(1000);
        	erpProductCRUDPage.GetEditButton().click();
        } else if (button.equals("Save Changes")) {
        	Thread.sleep(1000);
        	erpProductCRUDPage.GetSaveChangesButton().click();
        }
    }
	
//	@And("^User verify that \"([^\"]*)\" is the \"([^\"]*)\" in Product CRUD Page$")
//    public void user_verify_that_something_is_the_something_in_product_crud_page(String prodDetails, String field) throws Throwable {
//        if (field.equals("Product Name")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetProductNameTextbox().getAttribute("value"));
//        } else if (field.equals("Generic Name")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetGenericNameTextbox().getAttribute("value"));
//        } else if (field.equals("Product Segment")) {
//        	Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
//        	WebElement option = prodSegDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Product Sub-Segment")) {
//        	Select prodSubSegDropDown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
//        	WebElement option = prodSubSegDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Product Group")) {
//        	Select prodGroupDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
//        	WebElement option = prodGroupDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Product Sub-Group")) {
//        	Select prodSubGroupDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
//        	WebElement option = prodSubGroupDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Supplier")) {
//        	Select prodSupplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
//        	WebElement option = prodSupplierDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Manufacturer")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetManufacturerTextbox().getAttribute("value"));
//        } else if (field.equals("CPR Number")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetCPRNoTextbox().getAttribute("value"));
//        } else if (field.equals("CPR Expiry")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetCPRExpiryTextbox().getAttribute("value"));
//        } else if (field.equals("Stock Level")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetStockLevelTextbox().getAttribute("value"));
//        } else if (field.equals("Shelf Life")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetShelfLifeTextbox().getAttribute("value"));
//        } else if (field.equals("Minimum Order Quantity")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetMinOrderQtyTextbox().getAttribute("value"));
//        } else if (field.equals("Safety Stock Level")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetSafetyStockLevelTextbox().getAttribute("value"));
//        } else if (field.equals("Forecast")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetForecastTextbox().getAttribute("value"));
//        } else if (field.equals("Cost Per Unit")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetCostPerUnitTextbox().getAttribute("value"));
//        } else if (field.equals("Cost Per Box")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetCostPerBoxTextbox().getAttribute("value"));
//        } else if (field.equals("Selling Price Per Box")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetSellingPricePerBoxTextbox().getAttribute("value"));
//        } else if (field.equals("Packaging")) {
//        	Select prodPackagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
//        	WebElement option = prodPackagingDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Packaging Description")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetPackagingDescriptionTextbox().getAttribute("value"));
//        } else if (field.equals("Packaging")) {
//        	Select prodUnitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
//        	WebElement option = prodUnitOfMeasureDropDown.getFirstSelectedOption();
//        	String selectedOption = option.getText();
//        	Assert.assertEquals(prodDetails, selectedOption);
//        } else if (field.equals("Qty Per Pack")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetQtyPerPackTextbox().getAttribute("value"));
//        } else if (field.equals("Qty Per Pack")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetQtyPerPackTextbox().getAttribute("value"));
//        } else if (field.equals("Size Small")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetSizeSmallTextbox().getAttribute("value"));
//        } else if (field.equals("Size Big")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetSizeBigTextbox().getAttribute("value"));
//        } else if (field.equals("Weight Net")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetWeightNetTextbox().getAttribute("value"));
//        } else if (field.equals("Weight Gross")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetWeightGrossTextbox().getAttribute("value"));
//        } else if (field.equals("Temperature Min")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetTemperatureMinTextbox().getAttribute("value"));
//        } else if (field.equals("Temperature Max")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetTemperatureMaxTextbox().getAttribute("value"));
//        } else if (field.equals("Humidity")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetHumidityTextbox().getAttribute("value"));
//        } else if (field.equals("Storage Requirement")) {
//        	Assert.assertEquals(prodDetails, erpProductCRUDPage.GetStorageRequirementTextbox().getAttribute("value"));
//        }
//    }
	
//	@Then("^An error message is displayed in Product CRUD page$")
//    public void an_error_message_is_displayed_in_product_crud_page() throws Throwable {
//        Assert.assertTrue(erpProductCRUDPage.GetErrorMessage().isDisplayed());
//    }
	
	@And("^A duplicate product name error message is displayed$")
    public void a_duplicate_product_name_error_message_is_displayed() throws Throwable {
        String errorContent = erpProductCRUDPage.GetErrorMessageContent().getText();
        if (errorContent.contains("The product name has already been taken")) {
        	System.out.println("Error message duplicate appeared.");
        }
    }
	
	@And("^User input the \"([^\"]*)\" product test data$")
    public void user_input_the_something_product_test_data(String crudProducts) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 60);
        if (crudProducts.equals("Create")) {
        	File desktop = new File(System.getProperty("user.home"), "Desktop");
    		FileInputStream inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Create_Products.xlsx");
    	    XSSFWorkbook wB = new XSSFWorkbook(inputStream);
    		XSSFSheet sheet = wB.getSheetAt(0);
    		
    		int colCount = sheet.getRow(0).getLastCellNum();
    		int rowCount = sheet.getLastRowNum();
    		
    		System.out.println("colCount: " + colCount);
    		System.out.println("rowCount: " + rowCount);
    		
    		for (int x = 1; x <= rowCount; x++) {
    			erpProductListPage.GetCreateNewProductButton().click();
    			for (int y = 0; y < colCount; y++) {
    				XSSFCell cell = sheet.getRow(x).getCell(y);
    				String conNumtoString = "";
//    				try {
    					if (y == 0) {
    						erpProductCRUDPage.GetProductNameTextbox().clear();
    						erpProductCRUDPage.GetProductNameTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 1) {
    						erpProductCRUDPage.GetGenericNameTextbox().clear();
    						erpProductCRUDPage.GetGenericNameTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 2) {
    						Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
    			        	prodSegDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 3) {
    						Select prodSubSegDropdown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
    			        	prodSubSegDropdown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 4) {
    						Select prodGrpDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
    			        	prodGrpDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 5) {
    						Select prodSubGrpDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
    			        	prodSubGrpDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 6) {
    						Select supplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
    			        	supplierDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 7) {
    						erpProductCRUDPage.GetManufacturerTextbox().clear();
        					erpProductCRUDPage.GetManufacturerTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 8) {
    						erpProductCRUDPage.GetCPRNoTextbox().clear();
    						erpProductCRUDPage.GetCPRNoTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 9) {
    						erpProductCRUDPage.GetCPRExpiryTextbox().clear();
    						List<String> completeDate = Arrays.asList(cell.getStringCellValue().split(" "));
    						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(0));
    						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(Keys.TAB);
    						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(1));
    						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(2));
    					} else if (y == 10) {
//    						erpProductCRUDPage.GetCPRDocumentUpload().sendKeys("");
    					} else if (y == 11) {
//    						conNumtoString = Double.toString(cell.getNumericCellValue());
    						erpProductCRUDPage.GetShelfLifeTextbox().clear();
    						erpProductCRUDPage.GetShelfLifeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 12) {
    						erpProductCRUDPage.GetMinOrderQtyTextbox().clear();
    						erpProductCRUDPage.GetMinOrderQtyTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 13) {
    						erpProductCRUDPage.GetSafetyStockLevelTextbox().clear();
    						erpProductCRUDPage.GetSafetyStockLevelTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 14) {
    						erpProductCRUDPage.GetForecastTextbox().clear();
    						erpProductCRUDPage.GetForecastTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 15) {
    						erpProductCRUDPage.GetSupplierLeadTimeTextbox().clear();
    						erpProductCRUDPage.GetSupplierLeadTimeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 16) {
    						if (cell.getStringCellValue().equals("Yes")) {
    							erpProductCRUDPage.GetVATExemptCheckbox().click();
    						}
    					} else if (y == 17) {
    						erpProductCRUDPage.GetCostPerBoxTextbox().clear();
    						erpProductCRUDPage.GetCostPerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 18) {
    						erpProductCRUDPage.GetSellingPricePerBoxTextbox().clear();
    						erpProductCRUDPage.GetSellingPricePerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 19) {
    						erpProductCRUDPage.GetPackagingTab().click();
    						Select packagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
    						packagingDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 20) {
    						erpProductCRUDPage.GetPackagingDescriptionTextbox().clear();
    						erpProductCRUDPage.GetPackagingDescriptionTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 21) {
    						Select unitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
    			        	unitOfMeasureDropDown.selectByVisibleText(cell.getStringCellValue());
    					} else if (y == 22) {
    						erpProductCRUDPage.GetQtyPerPackTextbox().clear();
    						erpProductCRUDPage.GetQtyPerPackTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 23) {
    						erpProductCRUDPage.GetSizeSmallTextbox().clear();
    						erpProductCRUDPage.GetSizeSmallTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 24) {
    						erpProductCRUDPage.GetSizeBigTextbox().clear();
    						erpProductCRUDPage.GetSizeBigTextbox().sendKeys(cell.getStringCellValue());
    					} else if (y == 25) {
    						erpProductCRUDPage.GetWeightNetTextbox().clear();
    						erpProductCRUDPage.GetWeightNetTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 26) {
    						erpProductCRUDPage.GetWeightGrossTextbox().clear();
    						erpProductCRUDPage.GetWeightGrossTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 27) {
    						erpProductCRUDPage.GetTemperatureMinTextbox().clear();
    						erpProductCRUDPage.GetTemperatureMinTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 28) {
    						erpProductCRUDPage.GetTemperatureMaxTextbox().clear();
    						erpProductCRUDPage.GetTemperatureMaxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 29) {
    						erpProductCRUDPage.GetHumidityTextbox().clear();
    						erpProductCRUDPage.GetHumidityTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    					} else if (y == 30) {
    						erpProductCRUDPage.GetStorageRequirementTextbox().clear();
    						erpProductCRUDPage.GetStorageRequirementTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
    						erpProductCRUDPage.GetSubmitButton().click();
    				        erpProductCRUDPage.GetOKConfirmButton().click();
    					} 
//    				} catch (Exception e) {
//    					
//    				}
    			}
    		}
        } else if (crudProducts.equals("Edit")) {
        	File desktop = new File(System.getProperty("user.home"), "Desktop");
    		FileInputStream inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Edit_Products.xlsx");
    	    XSSFWorkbook wB = new XSSFWorkbook(inputStream);
    		XSSFSheet sheet = wB.getSheetAt(0);
    		
    		int colCount = sheet.getRow(0).getLastCellNum();
    		int rowCount = sheet.getLastRowNum();
    		
    		for (int x = 1; x <= rowCount; x++) {
    			
    			for (int y = 0; y < colCount; y++) {
    				XSSFCell cell = sheet.getRow(x).getCell(y);
    				if (x == 1 && y == 0) {
    					erpProductListPage.GetSearchBox().clear();
	    				erpProductListPage.GetSearchBox().sendKeys(cell.getStringCellValue());
	    				
						List<WebElement> prodListDataTable = erpProductListPage.GetProductListDataTable().findElements(By.cssSelector("div"));
		    			String prodCode = prodListDataTable.get(1).getText();
		    			erpProductListPage.GetProductCodeLink(prodCode).click();
		    			
		    			Thread.sleep(1000);
		    			
		    			erpProductCRUDPage.GetEditButton().click();
	    				
	    			} else if (x > 1 && y == 0) {
	    				erpProductCRUDPage.GetSearchCRUDPage().clear();
	    				erpProductCRUDPage.GetSearchCRUDPage().sendKeys(cell.getStringCellValue());
	    				
	    				Thread.sleep(2000);
	    				List<WebElement> prodCRUDDataTable = erpProductListPage.GetProductCRUDDataTable().findElements(By.cssSelector("div"));
	    				
	    				String prodCodeCRUD = prodCRUDDataTable.get(0).getText();
	    				String prodCodeCRUDnospace = prodCodeCRUD.replace(" ", "");
	    				
	    				List<String> prodCodeOnly = Arrays.asList(prodCodeCRUDnospace.split("-"));
	    				erpProductListPage.GetProductCodeLink(prodCodeOnly.get(0) + "-" + prodCodeOnly.get(1)).click();
	    				
	    				erpProductCRUDPage.GetEditButton().click();
	    			}
    				
    				if (y == 0) {
						erpProductCRUDPage.GetProductNameTextbox().clear();
						erpProductCRUDPage.GetProductNameTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 1) {
						erpProductCRUDPage.GetGenericNameTextbox().clear();
						erpProductCRUDPage.GetGenericNameTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 2) {
						Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
			        	prodSegDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 3) {
						Select prodSubSegDropdown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
			        	prodSubSegDropdown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 4) {
						Select prodGrpDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
			        	prodGrpDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 5) {
						Select prodSubGrpDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
			        	prodSubGrpDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 6) {
						Select supplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
			        	supplierDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 7) {
						erpProductCRUDPage.GetManufacturerTextbox().clear();
    					erpProductCRUDPage.GetManufacturerTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 8) {
						erpProductCRUDPage.GetCPRNoTextbox().clear();
						erpProductCRUDPage.GetCPRNoTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 9) {
						erpProductCRUDPage.GetCPRExpiryTextbox().clear();
						List<String> completeDate = Arrays.asList(cell.getStringCellValue().split(" "));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(0));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(Keys.TAB);
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(1));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(2));
					} else if (y == 10) {
//						erpProductCRUDPage.GetCPRDocumentUpload().sendKeys("");
					} else if (y == 11) {
//						conNumtoString = Double.toString(cell.getNumericCellValue());
						erpProductCRUDPage.GetShelfLifeTextbox().clear();
						erpProductCRUDPage.GetShelfLifeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 12) {
						erpProductCRUDPage.GetMinOrderQtyTextbox().clear();
						erpProductCRUDPage.GetMinOrderQtyTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 13) {
						erpProductCRUDPage.GetSafetyStockLevelTextbox().clear();
						erpProductCRUDPage.GetSafetyStockLevelTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 14) {
						erpProductCRUDPage.GetForecastTextbox().clear();
						erpProductCRUDPage.GetForecastTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 15) {
						erpProductCRUDPage.GetSupplierLeadTimeTextbox().clear();
						erpProductCRUDPage.GetSupplierLeadTimeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 16) {
						if (cell.getStringCellValue().equals("Yes")) {
							if (!erpProductCRUDPage.GetVATExemptCheckbox().isSelected()) {
								erpProductCRUDPage.GetVATExemptCheckbox().click();
							}
						} else if (cell.getStringCellValue().equals("No")) {
							if (erpProductCRUDPage.GetVATExemptCheckbox().isSelected()) {
								erpProductCRUDPage.GetVATExemptCheckbox().click();
							}
						}
					} else if (y == 17) {
						erpProductCRUDPage.GetCostPerBoxTextbox().clear();
						erpProductCRUDPage.GetCostPerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 18) {
						erpProductCRUDPage.GetSellingPricePerBoxTextbox().clear();
						erpProductCRUDPage.GetSellingPricePerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 19) {
						erpProductCRUDPage.GetPackagingTab().click();
						Select packagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
						packagingDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 20) {
						erpProductCRUDPage.GetPackagingDescriptionTextbox().clear();
						erpProductCRUDPage.GetPackagingDescriptionTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 21) {
						Select unitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
			        	unitOfMeasureDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 22) {
						erpProductCRUDPage.GetQtyPerPackTextbox().clear();
						erpProductCRUDPage.GetQtyPerPackTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 23) {
						erpProductCRUDPage.GetSizeSmallTextbox().clear();
						erpProductCRUDPage.GetSizeSmallTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 24) {
						erpProductCRUDPage.GetSizeBigTextbox().clear();
						erpProductCRUDPage.GetSizeBigTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 25) {
						erpProductCRUDPage.GetWeightNetTextbox().clear();
						erpProductCRUDPage.GetWeightNetTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 26) {
						erpProductCRUDPage.GetWeightGrossTextbox().clear();
						erpProductCRUDPage.GetWeightGrossTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 27) {
						erpProductCRUDPage.GetTemperatureMinTextbox().clear();
						erpProductCRUDPage.GetTemperatureMinTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 28) {
						erpProductCRUDPage.GetTemperatureMaxTextbox().clear();
						erpProductCRUDPage.GetTemperatureMaxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 29) {
						erpProductCRUDPage.GetHumidityTextbox().clear();
						erpProductCRUDPage.GetHumidityTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 30) {
						erpProductCRUDPage.GetStorageRequirementTextbox().clear();
						erpProductCRUDPage.GetStorageRequirementTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
						erpProductCRUDPage.GetSaveChangesButton().click();
				        erpProductCRUDPage.GetOKConfirmButton().click();
					} 
    			}
    		}
        }
    }
	
	@Then("^The product \"([^\"]*)\" is successfully created$")
    public void the_product_something_is_successfully_created(String crudProducts) throws Throwable {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		if (erpProductListPage.GetProductListTableHeader().isDisplayed()) {
			Thread.sleep(1000);
			
			File desktop = new File(System.getProperty("user.home"), "Desktop");
			
			FileInputStream inputStream = null;
			
    		if (crudProducts.equals("Created")) {
    			inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Create_Products.xlsx");
    		} else if (crudProducts.equals("Edited")) {
    			inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Edit_Products.xlsx");
    		}
    		
    	    XSSFWorkbook wB = new XSSFWorkbook(inputStream);
    		XSSFSheet sheet = wB.getSheetAt(0);
    		
    		int colCount = sheet.getRow(0).getLastCellNum();
    		int rowCount = sheet.getLastRowNum();
    		
    		System.out.println("colCount: " + colCount);
    		System.out.println("rowCount: " + rowCount);
    		
    		String conNumtoString = "";
    		
    		for (int x = 1; x <= rowCount; x++) {
    			for (int y = 0; y < colCount; y++) {
    				XSSFCell cell = sheet.getRow(x).getCell(y);
    				 
    				if ((x > 1 && y == 0) || (x == 1 && y == 0)) {
	    				erpProductCRUDPage.GetSearchCRUDPage().clear();
	    				erpProductCRUDPage.GetSearchCRUDPage().sendKeys(cell.getStringCellValue());
	    				
	    				Thread.sleep(2000);
	    				List<WebElement> prodCRUDDataTable = erpProductListPage.GetProductCRUDDataTable().findElements(By.cssSelector("div"));
	    				
	    				String prodCodeCRUD = prodCRUDDataTable.get(0).getText();
	    				String prodCodeCRUDnospace = prodCodeCRUD.replace(" ", "");
	    				
	    				List<String> prodCodeOnly = Arrays.asList(prodCodeCRUDnospace.split("-"));
	    				erpProductListPage.GetProductCodeLink(prodCodeOnly.get(0) + "-" + prodCodeOnly.get(1)).click();
	    			}
	    				
    				try {
    					if (y == 0) {
    						Thread.sleep(1000);
    						erpProductCRUDPage.GetProductInformationTab().click();
    						
    	    				Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetProductNameTextbox().getAttribute("value"));
    					} else if (y == 1) {
    						Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetGenericNameTextbox().getAttribute("value"));
    					} else if (y == 2) {
    						Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
	    		        	WebElement option = prodSegDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 3) {
    						Select prodSubSegDropDown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
	    		        	WebElement option = prodSubSegDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 4) {
    						Select prodGroupDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
	    		        	WebElement option = prodGroupDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 5) {
    						Select prodSubGroupDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
	    		        	WebElement option = prodSubGroupDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 6) {
    						Select supplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
	    		        	WebElement option = supplierDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 7) {
    						Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetManufacturerTextbox().getAttribute("value"));
    					} else if (y == 8) {
    						Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetCPRNoTextbox().getAttribute("value"));
    					} else if (y == 9) {
    						String cprExpiryDateUI = erpProductCRUDPage.GetCPRExpiryTextbox().getAttribute("value");
    						List<String> completeDate = Arrays.asList(cprExpiryDateUI.split("-"));
    						ArrayList<String> newDateUI = new ArrayList<String>();
    						
    						if (completeDate.get(1).equals("1")) {
    							newDateUI.add("January");
    						} else if (completeDate.get(1).equals("2")) {
    							newDateUI.add("February");
    						} else if (completeDate.get(1).equals("3")) {
    							newDateUI.add("March");
    						} else if (completeDate.get(1).equals("4")) {
    							newDateUI.add("April");
    						} else if (completeDate.get(1).equals("5")) {
    							newDateUI.add("May");
    						} else if (completeDate.get(1).equals("6")) {
    							newDateUI.add("June");
    						} else if (completeDate.get(1).equals("7")) {
    							newDateUI.add("July");
    						} else if (completeDate.get(1).equals("8")) {
    							newDateUI.add("August");
    						} else if (completeDate.get(1).equals("9")) {
    							newDateUI.add("September");
    						} else if (completeDate.get(1).equals("10")) {
    							newDateUI.add("October");
    						} else if (completeDate.get(1).equals("11")) {
    							newDateUI.add("November");
    						} else if (completeDate.get(1).equals("12")) {
    							newDateUI.add("December");
    						} 
    						
    						newDateUI.add(completeDate.get(2));
    						newDateUI.add(completeDate.get(0));
    						String finalDateUI = newDateUI.get(0) + " " + newDateUI.get(1) + " " + newDateUI.get(2);
    						
    						Assert.assertEquals(cell.getStringCellValue(), finalDateUI);
    					} 
    					else if (y == 10) {
//	    						Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetCPRDocumentUpload().getAttribute("value"));
    					} else if (y == 11) {
//	    						conNumtoString = Double.toString(cell.getNumericCellValue());
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetShelfLifeTextbox().getAttribute("value"));
    					} else if (y == 12) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetMinOrderQtyTextbox().getAttribute("value"));
    					} else if (y == 13) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetSafetyStockLevelTextbox().getAttribute("value"));
    					} else if (y == 14) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetForecastTextbox().getAttribute("value"));
    					} else if (y == 15) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetSupplierLeadTimeTextbox().getAttribute("value"));
    					} else if (y == 16) {
    						if (cell.getStringCellValue().equals("Yes")) {
    							Assert.assertTrue(erpProductCRUDPage.GetVATExemptCheckbox().isSelected());
    						} else if (cell.getStringCellValue().equals("No")) {
    							Assert.assertTrue(!erpProductCRUDPage.GetVATExemptCheckbox().isSelected());
    						}
    					} else if (y == 17) {
    						conNumtoString = Double.toString(cell.getNumericCellValue());
    						Assert.assertEquals(conNumtoString + "0", erpProductCRUDPage.GetCostPerBoxTextbox().getAttribute("value"));
    					} else if (y == 18) {
    						conNumtoString = Double.toString(cell.getNumericCellValue());
    						Assert.assertEquals(conNumtoString + "0", erpProductCRUDPage.GetSellingPricePerBoxTextbox().getAttribute("value"));
    					} else if (y == 19) {
    						erpProductCRUDPage.GetPackagingTab().click();
    						
    						Select packagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
	    		        	WebElement option = packagingDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 20) {
    						Assert.assertEquals(cell.getStringCellValue(), erpProductCRUDPage.GetPackagingDescriptionTextbox().getAttribute("value"));
    					} else if (y == 21) {
    						Select unitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
	    		        	WebElement option = unitOfMeasureDropDown.getFirstSelectedOption();
	    		        	String selectedOption = option.getText();
	    		        	Assert.assertEquals(cell.getStringCellValue(), selectedOption);
    					} else if (y == 22) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetQtyPerPackTextbox().getAttribute("value"));
    					} else if (y == 23) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetSizeSmallTextbox().getAttribute("value"));
    					} else if (y == 24) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetSizeBigTextbox().getAttribute("value"));
    					} else if (y == 25) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetWeightNetTextbox().getAttribute("value"));
    					} else if (y == 26) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetWeightGrossTextbox().getAttribute("value"));
    					} else if (y == 27) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetTemperatureMinTextbox().getAttribute("value"));
    					} else if (y == 28) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetTemperatureMaxTextbox().getAttribute("value"));
    					} else if (y == 29) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetHumidityTextbox().getAttribute("value"));
    					} else if (y == 30) {
    						Assert.assertEquals(new DataFormatter().formatCellValue(cell), erpProductCRUDPage.GetStorageRequirementTextbox().getAttribute("value"));
    					} 
    				} catch (Exception e) {
    					
    				}
    			}
    		}
		}
		
    }
	
	@Then("^User validated the CRUD page using the \"([^\"]*)\" test data$")
    public void user_validated_the_crud_page_using_the_something_test_data(String crudValidation) throws Throwable {
		File desktop = new File(System.getProperty("user.home"), "Desktop");		
		FileInputStream inputStream = null;
		
		if (crudValidation.equals("Create Validate")) {
			inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Create_Validate_Products.xlsx");
		} else if (crudValidation.equals("Edit Validate")) {
			inputStream = new FileInputStream(desktop + File.separator + "ERP_Test_Data" + File.separator + "Product_DB" + File.separator + "Edit_Validate_Products.xlsx");
		}
		
	    XSSFWorkbook wB = new XSSFWorkbook(inputStream);
		XSSFSheet sheet = wB.getSheetAt(0);
		
		int colCount = sheet.getRow(0).getLastCellNum();
		int rowCount = sheet.getLastRowNum();
		
		System.out.println("colCount: " + colCount);
		System.out.println("rowCount: " + rowCount);
		
		for (int x = 1; x <= rowCount; x++) {
			
			
			for (int y = 0; y < colCount; y++) {
				try {
					XSSFCell cell = sheet.getRow(x).getCell(y);
					
					if (crudValidation.equals("Create Validate") && y == 0) {
						erpProductListPage.GetCreateNewProductButton().click();
					} else if (crudValidation.equals("Edit Validate")) {
						
					}
					
					if (y == 0) {
						erpProductCRUDPage.GetProductNameTextbox().clear();
						erpProductCRUDPage.GetProductNameTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 1) {
						erpProductCRUDPage.GetGenericNameTextbox().clear();
						erpProductCRUDPage.GetGenericNameTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 2) {
						Select prodSegDropDown = new Select(erpProductCRUDPage.GetProductSegmentDropdown());
			        	prodSegDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 3) {
						Select prodSubSegDropdown = new Select(erpProductCRUDPage.GetProductSubSegmentDropdown());
			        	prodSubSegDropdown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 4) {
						Select prodGrpDropDown = new Select(erpProductCRUDPage.GetProductGroupDropdown());
			        	prodGrpDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 5) {
						Select prodSubGrpDropDown = new Select(erpProductCRUDPage.GetProductSubGroupDropdown());
			        	prodSubGrpDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 6) {
						Select supplierDropDown = new Select(erpProductCRUDPage.GetSupplierDropdown());
			        	supplierDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 7) {
						erpProductCRUDPage.GetManufacturerTextbox().clear();
						erpProductCRUDPage.GetManufacturerTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 8) {
						erpProductCRUDPage.GetCPRNoTextbox().clear();
						erpProductCRUDPage.GetCPRNoTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 9) {
						erpProductCRUDPage.GetCPRExpiryTextbox().clear();
						List<String> completeDate = Arrays.asList(cell.getStringCellValue().split(" "));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(0));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(Keys.TAB);
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(1));
						erpProductCRUDPage.GetCPRExpiryTextbox().sendKeys(completeDate.get(2));
					} else if (y == 10) {
//							erpProductCRUDPage.GetCPRDocumentUpload().sendKeys("");
					} else if (y == 11) {
						erpProductCRUDPage.GetShelfLifeTextbox().clear();
						erpProductCRUDPage.GetShelfLifeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 12) {
						erpProductCRUDPage.GetMinOrderQtyTextbox().clear();
						erpProductCRUDPage.GetMinOrderQtyTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 13) {
						erpProductCRUDPage.GetSafetyStockLevelTextbox().clear();
						erpProductCRUDPage.GetSafetyStockLevelTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 14) {
						erpProductCRUDPage.GetForecastTextbox().clear();
						erpProductCRUDPage.GetForecastTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 15) {
						erpProductCRUDPage.GetSupplierLeadTimeTextbox().clear();
						erpProductCRUDPage.GetSupplierLeadTimeTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 16) {
						if (cell.getStringCellValue().equals("Yes")) {
							erpProductCRUDPage.GetVATExemptCheckbox().click();
						}
					} else if (y == 17) {
						erpProductCRUDPage.GetCostPerBoxTextbox().clear();
						erpProductCRUDPage.GetCostPerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 18) {
						erpProductCRUDPage.GetSellingPricePerBoxTextbox().clear();
						erpProductCRUDPage.GetSellingPricePerBoxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 19) {
						erpProductCRUDPage.GetPackagingTab().click();
						Select packagingDropDown = new Select(erpProductCRUDPage.GetPackagingDropdown());
						packagingDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 20) {
						erpProductCRUDPage.GetPackagingDescriptionTextbox().clear();
						erpProductCRUDPage.GetPackagingDescriptionTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 21) {
						Select unitOfMeasureDropDown = new Select(erpProductCRUDPage.GetUnitOfMeasureDropdown());
			        	unitOfMeasureDropDown.selectByVisibleText(cell.getStringCellValue());
					} else if (y == 22) {
						erpProductCRUDPage.GetQtyPerPackTextbox().clear();
						erpProductCRUDPage.GetQtyPerPackTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 23) {
						erpProductCRUDPage.GetSizeSmallTextbox().clear();
						erpProductCRUDPage.GetSizeSmallTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 24) {
						erpProductCRUDPage.GetSizeBigTextbox().clear();
						erpProductCRUDPage.GetSizeBigTextbox().sendKeys(cell.getStringCellValue());
					} else if (y == 25) {
						erpProductCRUDPage.GetWeightNetTextbox().clear();
						erpProductCRUDPage.GetWeightNetTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 26) {
						erpProductCRUDPage.GetWeightGrossTextbox().clear();
						erpProductCRUDPage.GetWeightGrossTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 27) {
						erpProductCRUDPage.GetTemperatureMinTextbox().clear();
						erpProductCRUDPage.GetTemperatureMinTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 28) {
						erpProductCRUDPage.GetTemperatureMaxTextbox().clear();
						erpProductCRUDPage.GetTemperatureMaxTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 29) {
						erpProductCRUDPage.GetHumidityTextbox().clear();
						erpProductCRUDPage.GetHumidityTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
					} else if (y == 30) {
						erpProductCRUDPage.GetStorageRequirementTextbox().clear();
						erpProductCRUDPage.GetStorageRequirementTextbox().sendKeys(new DataFormatter().formatCellValue(cell));
						erpProductCRUDPage.GetSubmitButton().click();
						Assert.assertTrue(erpProductCRUDPage.GetErrorMessage().isDisplayed());
						erpProductCRUDPage.GetOKConfirmButton().click();
						erpProductCRUDPage.GetXButton().click();
					}
				} catch (Exception e) {
					
				}
				
			}
		}
    }

}
