package stepDefinitions.Customer_DB;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import stepDefinitions.Test_Base;

public class CustomerInformationPage extends Test_Base {
	
	 @Then("^User click \"([^\"]*)\" link in Customer List page$")
	    public void user_click_something_link_in_customer_list_page(String name) throws Throwable {
		 
		 erpCustomerListPage.GetSearchBar().sendKeys(name);
		 
		 if (erpCustomerListPage.GetCustomerName(name).isDisplayed()) {
			 erpCustomerListPage.GetCustomerName(name).click();
			 
			 if (erpCustomerInformationPage.GetEditButton().isEnabled()) {
				 erpCustomerInformationPage.GetEditButton().click();
				 
				 Assert.assertEquals(name, erpCustomerInformationPage.GetCustomerName().getText());
			 }
			 
		 }
	 }
	 
	 @And("^User verify information displayed in Customer Info page$")
	    public void user_verify_information_displayed_in_customer_info_page() throws Throwable {
	        
		 if (erpCustomerInformationPage.GetEditButton().isEnabled()) {
			 erpCustomerInformationPage.GetEditButton().click();
			 
			 Assert.assertTrue(null, false);
		 }
    }
	 
	 

}
