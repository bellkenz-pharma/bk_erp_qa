package stepDefinitions.Customer_DB;

import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import stepDefinitions.Test_Base;

public class CustomerListPage extends Test_Base {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	@When("^User successfully accessed the Customer List page$")
    public void user_successfully_accessed_the_customer_list_page() throws Throwable {
		
		if (erpCustomerListPage.GetCustomerCenterHeader().isDisplayed()) {
			Assert.assertEquals("Customers Center", erpCustomerListPage.GetCustomerCenterHeader().getText());
		}
		
    }
	
	@Then("^The \"([^\"]*)\" button is displayed in Customer List page$")
    public void the_something_button_is_displayed_in_customer_list_page(String button) throws Throwable {
        
		if (button.equals("Create New Customer")) {
			
			if	(erpCustomerListPage.GetCreateNewCustomerButton().isDisplayed()) {
				String label = erpCustomerListPage.GetCreateNewCustomerButton().getText();
				
				if (button.equals(label)) {
					System.out.println(label + " button was displayed");
				} else {
					System.out.println("Button was not  displayed");
				}
			}
		} 
		
		else if (button.equalsIgnoreCase("Columns")) {
			
			if (erpCustomerListPage.GetColumnsButton().isDisplayed()) {
				String label = erpCustomerListPage.GetColumnsButton().getText();
				if (button.equalsIgnoreCase(label)) {
					System.out.println(label + " button was displayed");
				} else {
					System.out.println("Button was not  displayed");
				}
			}
		}
		
		else if (button.equals("Search")) {
			
			if (erpCustomerListPage.GetSearchBar().isDisplayed()) { 
				String label = erpCustomerListPage.GetSearchBar().getAttribute("placeholder").replace("…", "");
				
				if (button.equalsIgnoreCase(label)) {
					System.out.println(label + " button was displayed");
				} else {
					System.out.println("Button was not  displayed");
					System.out.println(label);
				}
			} 
		}
    }

    @And("^The \"([^\"]*)\" table column is displayed in Customer List page$")
    public void the_something_table_column_is_displayed_in_customer_list_page(String col) throws Throwable {
        
    	String column;
    	String col1 = "customer_code";
    		
    	if (col.equals("Customer Code")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders(col1).getText();
    		erpCustomerListPage.GetCustomerListColumnHeaders(col1).click();
    		System.out.println(column+" is displayed");
    	}
    		
    	else if (col.equals("Customer Name")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("customer_name").getText();
    		System.out.println(column+" is displayed");
    	}
    		
    	else if (col.equals("Tin")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("tin_no").getText();
    		System.out.println(column+" is displayed");
    	}
    		
    	else if (col.equals("Amount Outstanding")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("amount_outstanding").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("Terms")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("payment_terms_text_format").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("Channel")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("channel_text_format").getText();
    		System.out.println(column+" is displayed");
    	}
    		
    	else if (col.equals("Sales Manager")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("kam_name").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("Sales Officer")) {
    		
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").getText();
    		System.out.println(column+" is displayed");
    		
//    		if (erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").isDisplayed()==false) {
//    			ScrollLeft();
//    			column = erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").getText();
//        		System.out.println(column+" is displayed");
//    		}
    	}
    	
    	else if (col.equals("SAR")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("sar").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("HSAR")) {
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("hsar").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("Status")) {
    		ScrollLeft();
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("status").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("EWT")) {
    		ScrollLeft();
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("ewt").getText();
    		System.out.println(column+" is displayed");
    	}
    	
    	else if (col.equals("SC Discount")) {
    		ScrollLeft();
    		column = erpCustomerListPage.GetCustomerListColumnHeaders("sc_discount").getText();
    		
    		System.out.println(column+" is displayed");
    	}
    }
    
    @And("^User sort \"([^\"]*)\" column$")
    public void user_sort_something_column(String header) throws Throwable {
        
    	if (header.equalsIgnoreCase("Customer Code")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_code").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_code").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_code").click();
    	}
    	
    	else if (header.equals("Customer Name")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("customer_name").click();
    	}
    		
    	else if (header.equals("Tin")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("tin_no").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("tin_no").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("tin_no").click();
    		
    	}
    		
    	else if (header.equals("Amount Outstanding")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("amount_outstanding").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("amount_outstanding").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("amount_outstanding").click();
    	}
    	
    	else if (header.equals("Terms")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("payment_terms_text_format").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("payment_terms_text_format").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("payment_terms_text_format").click();
    	}
    	
    	else if (header.equals("Channel")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("channel_text_format").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("channel_text_format").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("channel_text_format").click();
    	}
    		
    	else if (header.equals("Sales Manager")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("kam_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("kam_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("kam_name").click();
    	}
    	
    	else if (header.equals("Sales Officer")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("kass_name").click();
    	}
    	
    	else if (header.equals("SAR")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("sar").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("sar").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("sar").click();
    	}
    	
    	else if (header.equals("HSAR")) {
    		erpCustomerListPage.GetCustomerListColumnHeaders("hsar").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("hsar").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("hsar").click();
    	}
    	
    	else if (header.equals("Status")) {
    		ScrollLeft();
    		erpCustomerListPage.GetCustomerListColumnHeaders("status").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("status").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("status").click();
    	}
    	
    	else if (header.equals("EWT")) {
    		ScrollLeft();
    		erpCustomerListPage.GetCustomerListColumnHeaders("ewt").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("ewt").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("ewt").click();
    	}
    	
    	else if (header.equals("SC Discount")) {
    		ScrollLeft();
    		erpCustomerListPage.GetCustomerListColumnHeaders("sc_discount").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("sc_discount").click();
    		erpCustomerListPage.GetCustomerListColumnHeaders("sc_discount").click();
    		
    	}
    }

    @Then("^User show \"([^\"]*)\" entries in Customer List page$")
    public void user_show_something_entries_in_customer_list_page(int value) throws Throwable {
        
    	Thread.sleep(2000);
    	System.out.println(erpCustomerListPage.GetEntriesLabel().getText());
    	erpCustomerListPage.GetShowEntries().click();
    	erpCustomerListPage.GetShowEntriesList(value).click();
    	System.out.println(value+" was selected");
    	
    }
    
    @And("^User view \"([^\"]*)\" Page in Customer List page$")
    public void user_view_something_page_in_customer_list_page(String page) throws Throwable {
        
    	erpCustomerListPage.GetPageControl(page).click();
    	System.out.println(page+" page clicked : "+erpCustomerListPage.GetEntriesLabel().getText());
    	
    	
    }

}
