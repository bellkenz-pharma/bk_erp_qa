package stepDefinitions;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import cucumberTest.Config;
import driverBuilder.ChromeDriverBuilder;
import pageObjects.GmailPage;
import pageObjects.Customer_DB.ERPCustomerInformationPage;
import pageObjects.Customer_DB.ERPCustomerListPage;
import pageObjects.Product_DB.ERPProductCRUDPage;
import pageObjects.Product_DB.ERPProductListPage;
import pageObjects.System_UI_Navigation.ERPForgotPasswordPage;
import pageObjects.System_UI_Navigation.ERPHomePage;
import pageObjects.System_UI_Navigation.ERPLoginPage;
import pageObjects.User_Role_Permission.ERPUserListPage;
import pageObjects.Warehouse_DB.ERPWarehouseCRUDPage;
import pageObjects.Warehouse_DB.ERPWarehouseListPage;
import utilities.GenericTasks;

public class Test_Base {
	
	protected static WebDriver driver;
	
	protected static GenericTasks genericTasks;
	protected static ERPLoginPage erpLoginPage;
	protected static ERPHomePage erpHomePage;
	protected static ERPForgotPasswordPage erpForgotPasswordPage;
	protected static GmailPage gmailPage;
	protected static ERPUserListPage erpUserListPage;
	protected static ERPProductListPage erpProductListPage;
	protected static ERPProductCRUDPage erpProductCRUDPage;
	protected static ERPCustomerListPage erpCustomerListPage;
	protected static ERPCustomerInformationPage erpCustomerInformationPage;
	protected static ERPWarehouseListPage erpWarehouseListPage;
	protected static ERPWarehouseCRUDPage erpWarehouseCRUDPage;
	
	public static void SetUp() {
		switch (Config.browserName) {
			case "chrome":
				driver = ChromeDriverBuilder.SetupChrome();
				break;
			default:
				ChromeDriverBuilder.SetupChrome();
		}
		
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		setupPageObjects();
		
	}
	
	public static void KillDriver() {
		System.out.println("[DEBUG] Quitting web driver instance");
		driver.quit();
	}
	
	public static WebDriver getDriver() {
		return driver;
	}
	
	public static void setupPageObjects() {
		genericTasks = new GenericTasks(getDriver());
		erpLoginPage = new ERPLoginPage(getDriver());
		erpHomePage = new ERPHomePage(getDriver());
		erpForgotPasswordPage = new ERPForgotPasswordPage(getDriver());
		gmailPage = new GmailPage(getDriver());
		erpUserListPage = new ERPUserListPage(getDriver());
		erpProductListPage = new ERPProductListPage(getDriver());
		erpProductCRUDPage = new ERPProductCRUDPage(getDriver());
		erpCustomerListPage = new ERPCustomerListPage(getDriver());
		erpCustomerInformationPage = new ERPCustomerInformationPage(getDriver());
		erpWarehouseListPage = new ERPWarehouseListPage(getDriver());
		erpWarehouseCRUDPage = new ERPWarehouseCRUDPage(getDriver());
	}
	
	public static void ScrollLeft() {
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,document.body.scrollHeight)");
		
		EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(driver);
		eventFiringWebDriver.executeScript("document.querySelector('div[class=\"MuiDataGrid-window\"]').scrollLeft=500");
		
	}
 
}
