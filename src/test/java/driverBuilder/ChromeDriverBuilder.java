package driverBuilder;

import cucumberTest.Config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class ChromeDriverBuilder {
	
	public static WebDriver SetupChrome() {
		try {
			Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
		} catch (Exception e) {
			System.out.println("Something went wrong.");
		}
		
		System.setProperty("webdriver.chrome.driver", Config.chromeDriverPath);
		
		ChromeOptions options = new ChromeOptions();
//		options.setBinary("C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);
		
		ChromeDriver driver = new ChromeDriver(capabilities);
		return driver;
	}
}
