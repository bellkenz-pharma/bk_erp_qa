package testRunners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/java/Features/",
		glue = "stepDefinitions", 
//		tags = "@UserListCRUDFeature",
//		tags = "@LoginFeature",
//		tags = "@CustomerList",
		tags = "@CustomerinformationView",
		plugin = {"pretty", "html:target/cucumber", "json:target/cucumber.json", "junit:target/cubes.xml"}
		)
public class TestRunner_ERP {
	
}
