package utilities;

import org.openqa.selenium.WebDriver;

import cucumberTest.Config;
import pageObjects.System_UI_Navigation.ERPLoginPage;

public class GenericTasks {
	
	private WebDriver Driver;
	
	public GenericTasks(WebDriver driver) {
		this.Driver = driver;
	}
	
	public void navigateToERPURL() {
		Driver.navigate().to(Config.baseURL);
	}
	
	public void navigateToGmailURL() {
		Driver.navigate().to(Config.gmailSite);
	}
	
	public void loginToERPAdmin() {
		ERPLoginPage erpLoginPage = new ERPLoginPage(Driver);
		
		navigateToERPURL();
		erpLoginPage.GetUsernameTextbox().clear();
		erpLoginPage.GetUsernameTextbox().sendKeys(Config.adminUsername);
		erpLoginPage.GetPasswordTextbox().clear();
		erpLoginPage.GetPasswordTextbox().sendKeys(Config.adminPassword);
		erpLoginPage.GetSignInButton().click();
	}
	
	public void loginToERPQATope() {
		ERPLoginPage erpLoginPage = new ERPLoginPage(Driver);
		
		navigateToERPURL();
		erpLoginPage.GetUsernameTextbox().clear();
		erpLoginPage.GetUsernameTextbox().sendKeys(Config.qaTope_Username);
		erpLoginPage.GetPasswordTextbox().clear();
		erpLoginPage.GetPasswordTextbox().sendKeys(Config.qaTope_Password);
		erpLoginPage.GetSignInButton().click();
		
	}

}
