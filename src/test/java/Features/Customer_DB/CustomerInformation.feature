#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@CustomerInformation
Feature: Customer Information feature

  @CustomerinformationView
  Scenario: Verify if user is able to view Customer Information
    Given User login as "admin" account
		And User click the "Master Databases" menu
		And User click the "Customer Databases" menu
		And User click the "Customer List" menu
		When User successfully accessed the Customer List page
		Then User click "Customer A" link in Customer List page
		And User verify information displayed in Customer Info page
		
	@CustomerinformationEdit
  Scenario: Verify if user is able to view Customer Information
    Given User login as "admin" account
		And User click the "Master Databases" menu
		And User click the "Customer Databases" menu
		And User click the "Customer List" menu
		When User successfully accessed the Customer List page
		Then User click "Customer A" link in Customer List page
		And User verify information displayed in Customer Info page
		And User click edit button in Customer Info page
		
		

