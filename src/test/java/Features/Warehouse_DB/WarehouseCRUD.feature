@WarehouseCRUD
Feature: Warehouse CRUD Feature

  Scenario: Verify if the user can add warehouse
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Warehouse Database" menu
    And User click the "Warehouse List" menu
    When User successfully accessed the "Warehouse List" page
    And User input the "Create" warehouse test data
    Then The warehouse "Created" is successfully created