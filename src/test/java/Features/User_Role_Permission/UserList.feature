@UserListFeature
Feature: User List Feature

  Scenario: Verify the correctness of User List UI
    Given User login as "Tope" account
    And User click the "User Control" menu
    When User click "User Management" tab
    Then "Add User" button is displayed
    And "Search User List" textbox is displayed
    And "User List" table is displayed
    And User List table has "Active" table header
    And User List table has "Employee No." table header
    And User List table has "First Name" table header
    And User List table has "Last Name" table header
    And User List table has "Email" table header
    And User List table has "User Name" table header
    And User List table has "Department" table header
    And User List table has "Job Title" table header
    And User List table has "Roles" table header
    And User List table has "Action" table header

  Scenario: Verify the correctness of Add User UI
    Given User login as "Tope" account
    And User click the "User Control" menu
    And User click "User Management" tab
    When User click the "Add User" button
    Then Add User has "Username" textbox
    And Add User has "Employee Number" textbox
    And Add User has "Last Name" textbox
    And Add User has "First Name" textbox
    And Add User has "Email" textbox
    And Add User has "Password" textbox
    And Add User has "Department" textbox
    And Add User has "Job" textbox
    And Add User has "Contact Number" textbox

  Scenario: Verify the correctness of Edit User UI
    Given User login as "Tope" account
    And User click the "User Control" menu
    And User click "User Management" tab
    When User click the "Edit" icon
    Then Edit User has "Username" textbox
    And Edit User has "Employee Number" textbox
    And Edit User has "Last Name" textbox
    And Edit User has "First Name" textbox
    And Edit User has "Email" textbox
    And Edit User has "Password" textbox
    And Edit User has "Department" textbox
    And Edit User has "Job" textbox
    And Edit User has "Contact Number" textbox

  Scenario: Verify the correctness of Change Password UI
    Given User login as "Tope" account
    And User click the "User Control" menu
    And User click "User Management" tab
    When User click the "Change Password" icon
    Then Change Password has "Current Password" textbox
    And Change Password has "New Password" textbox
    And Change Password has "Confirm Password" textbox
    
    
    
    
