@ProductList
Feature: Product List Feature

  Scenario: UI Verification
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Product Databases" menu
    And User click the "Product List" menu
    When User successfully accessed the "Product List" page
    Then Table header name is "List of Product Stocks" for Product List table
    And The "Create New Product" button is displayed in Product List page
    And The "Product Code" table column is displayed in Product List page
    And The "Product Name" table column is displayed in Product List page
    And The "Generic Name" table column is displayed in Product List page
    And The "Supplier" table column is displayed in Product List page
    And The "Product Segment" table column is displayed in Product List page
    And The "Product Group" table column is displayed in Product List page
    And The "Packaging" table column is displayed in Product List page
    And The "Qty Per Pack" table column is displayed in Product List page
    And The "Stock Level" table column is displayed in Product List page
    And The "CPR No." table column is displayed in Product List page
    And The "CPR Expiry" table column is displayed in Product List page
    And The "Active" table column is displayed in Product List page