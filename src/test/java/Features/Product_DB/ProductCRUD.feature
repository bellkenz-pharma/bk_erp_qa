@ProductCRUD
Feature: Product CRUD Feature

	#TS-2-1
  #Scenario: Verify if the user can add product and view - TC1
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #And User input the "Create" product test data
    #Then The product "Created" is successfully created

  #TS-2-2  
#	Scenario: Verify if the user can add product and view - TC2
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "Product Automation 1MG" as "Product Name" in Product CRUD page
    #And User input "Product Automation" as "Generic Name" in Product CRUD page
    #And User select "Neutraceuticals" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Orals" as "Product Group" in Product CRUD page
    #And User select "Oral" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu QC" as "Manufacturer" in Product CRUD page
    #And User input "1234-1234-1234" as "CPR Number" in Product CRUD page
    #And User input "October 22 2025" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "100" as "Stock Level" in Product CRUD page
    #And User input "24" as "Shelf Life" in Product CRUD page
    #And User input "10" as "Minimum Order Quantity" in Product CRUD page
    #And User input "30" as "Safety Stock Level" in Product CRUD page
    #And User input "40" as "Forecast" in Product CRUD page
    #And User input "30" as "Supplier Lead Time" in Product CRUD page
    #And User input "12" as "Cost Per Unit" in Product CRUD page
    #And User input "100" as "Cost Per Box" in Product CRUD page
    #And User input "200" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "Sachet" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "2 x 2 x 2" as "Size Small" in Product CRUD page
    #And User input "5 x 5 x 5" as "Size Big" in Product CRUD page
    #And User input "4" as "Weight Net" in Product CRUD page
    #And User input "5" as "Weight Gross" in Product CRUD page
    #And User input "20" as "Temperature Min" in Product CRUD page
    #And User input "24" as "Temperature Max" in Product CRUD page
    #And User input "40" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then The product named "Product Automation 1MG" is successfully created and displayed in Product List
    #Validation if all details are reflected correctly
    #And User click "Product Automation 1MG" product name in Product List Page
    #When User click "Edit" button in Product CRUD page
    #And User verify that "Product Automation 1MG" is the "Product Name" in Product CRUD Page
    #And User verify that "Product Automation" is the "Generic Name" in Product CRUD Page
    #And User verify that "Neutraceuticals" is the "Product Segment" in Product CRUD Page
    #And User verify that "" is the "Product Sub-Segment" in Product CRUD Page
    #And User verify that "Orals" is the "Product Group" in Product CRUD Page
    #And User verify that "" is the "Product Sub-Group" in Product CRUD Page
    #And User verify that "VNDR#-69 - wewe" is the "Supplier" in Product CRUD Page
    #And User verify that "Manu QC" is the "Manufacturer" in Product CRUD Page
    #And User verify that "1234-1234-1234" is the "CPR Number" in Product CRUD Page
    #And User verify that "October 22 2025" is the "CPR Expiry" in Product CRUD Page
    #And User verify that "100" is the "Stock Level" in Product CRUD Page
    #And User verify that "24" is the "Shelf Life" in Product CRUD Page
    #And User verify that "10" is the "Minimum Order Quantity" in Product CRUD Page
    #And User verify that "30" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "40" is the "Forecast" in Product CRUD Page
    #And User verify that "30" is the "Supplier Lead Time" in Product CRUD Page
    #And User verify that "12" is the "Cost Per Unit" in Product CRUD Page
    #And User verify that "100" is the "Cost Per Box" in Product CRUD Page
    #And User verify that "200" is the "Selling Price Per Box" in Product CRUD Page
    #And User click "Packaging" tab in Product CRUD page
    #And User verify that "Sachet" is the "Packaging" in Product CRUD Page
    #And User verify that "temp" is the "Packaging Description" in Product CRUD Page
    #And User verify that "" is the "Unit of Measure" in Product CRUD Page
    #And User verify that "10" is the "Qty Per Pack" in Product CRUD Page
    #And User verify that "2 x 2 x 2" is the "Size Small" in Product CRUD Page
    #And User verify that "5 x 5 x 5" is the "Size Big" in Product CRUD Page
    #And User verify that "4" is the "Weight Net" in Product CRUD Page
    #And User verify that "5" is the "Weight Gross" in Product CRUD Page
    #And User verify that "20" is the "Temperature Min" in Product CRUD Page
    #And User verify that "24" is the "Temperature Max" in Product CRUD Page
    #And User verify that "40" is the "Humidity" in Product CRUD Page
    #And User verify that "" is the "Storage Requirement" in Product CRUD Page
  
  #Scenario: Verify if the user can edit the product created - TC1
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #And User input the "Edit" product test data
    #Then The product "Edited" is successfully created
  
  #TS-3-1  
#	Scenario: Verify if the user can edit the product created - TC1
#		Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #And User click "" product name in Product List Page
    #And User input "Generic 09823" as "Generic Name" in Product CRUD page
    #And User select "Diabetes" as "Product Group" in Product CRUD page
    #And User input "0000-0000-0000-0000" as "CPR Number" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "18" as "Shelf Life" in Product CRUD page
    #And User input "100" as "Safety Stock Level" in Product CRUD page
    #And User input "20" as "Supplier Lead Time" in Product CRUD page
    #And User input "500" as "Selling Price Per Box" in Product CRUD page
    #When User click "Save Changes" button in Product CRUD page
    #And User click "" product name in Product List Page
    #Then User verify that "Generic 09823" is the "Generic Name" in Product CRUD Page
    #And User verify that "Diabetes" is the "Product Group" in Product CRUD Page
    #And User verify that "0000-0000-0000-0000" is the "CPR Number" in Product CRUD Page
    #And User verify that "18" is the "Shelf Life" in Product CRUD Page
    #And User verify that "100" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "20" is the "Supplier Lead Time" in Product CRUD Page
    #And User verify that "500" is the "Selling Price Per Box" in Product CRUD Page

  #TS-4-1 
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC1
  #	Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User click "Packaging" tab in Product CRUD page
    #And User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
  
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC2
  #	Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User validated the CRUD page using the "Create Validate" test data
    
  Scenario: Verify if the user cannot create a product when the required columns are blank - TC2
  	Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Product Databases" menu
    And User click the "Product List" menu
    When User successfully accessed the "Product List" page
    Then User validated the CRUD page using the "Edit Validate" test data
    
  #
  #TS-4-2  
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC2
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "" as "Product Name" in Product CRUD page
    #And User input "GenName001" as "Generic Name" in Product CRUD page
    #And User select "Vitamins" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Vials" as "Product Group" in Product CRUD page
    #And User select "Orals" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu Pasig" as "Manufacturer" in Product CRUD page
    #And User input "1234-1234-1234" as "CPR Number" in Product CRUD page
    #And User input "October 22 2025" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "20.3" as "Shelf Life" in Product CRUD page
    #And User input "10" as "Minimum Order Quantity" in Product CRUD page
    #And User input "30" as "Safety Stock Level" in Product CRUD page
    #And User input "40" as "Forecast" in Product CRUD page
    #And User input "" as "Supplier Lead Time" in Product CRUD page
    #And User input "12" as "Cost Per Unit" in Product CRUD page
    #And User input "100" as "Cost Per Box" in Product CRUD page
    #And User input "200" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "Sachet" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "2.25 x 2.25 x 2.75" as "Size Small" in Product CRUD page
    #And User input "5.1 x 5.2 x 5.3" as "Size Big" in Product CRUD page
    #And User input "4.57" as "Weight Net" in Product CRUD page
    #And User input "5.21" as "Weight Gross" in Product CRUD page
    #And User input "20.5" as "Temperature Min" in Product CRUD page
    #And User input "24.7" as "Temperature Max" in Product CRUD page
    #And User input "40.99" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
 #
 #TS-4-3   
 #Scenario: Verify if the user cannot create a product when the required columns are blank - TC3
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "ProdProdBK100mg" as "Product Name" in Product CRUD page
    #And User input "" as "Generic Name" in Product CRUD page
    #And User select "Hospital Care" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Nutra" as "Product Group" in Product CRUD page
    #And User select "Orals" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu Pasig" as "Manufacturer" in Product CRUD page
    #And User input "1234-1234-1234" as "CPR Number" in Product CRUD page
    #And User input "" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "20.3" as "Shelf Life" in Product CRUD page
    #And User input "123" as "Minimum Order Quantity" in Product CRUD page
    #And User input "" as "Safety Stock Level" in Product CRUD page
    #And User input "40" as "Forecast" in Product CRUD page
    #And User input "91" as "Supplier Lead Time" in Product CRUD page
    #And User input "" as "Cost Per Unit" in Product CRUD page
    #And User input "" as "Cost Per Box" in Product CRUD page
    #And User input "2000" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "" as "Size Small" in Product CRUD page
    #And User input "5.1 x 5.2 x 5.3" as "Size Big" in Product CRUD page
    #And User input "4.57" as "Weight Net" in Product CRUD page
    #And User input "5.21" as "Weight Gross" in Product CRUD page
    #And User input "20.5" as "Temperature Min" in Product CRUD page
    #And User input "24.7" as "Temperature Max" in Product CRUD page
    #And User input "40.99" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
  #
  #TS-4-4  
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC4
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "ProdName001" as "Product Name" in Product CRUD page
    #And User input "GenName001" as "Generic Name" in Product CRUD page
    #And User select "Nutraceutical" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Nutra" as "Product Group" in Product CRUD page
    #And User select "Orals" as "Product Sub-Group" in Product CRUD page
    #And User select "" as "Supplier" in Product CRUD page
    #And User input "Manu Makati" as "Manufacturer" in Product CRUD page
    #And User input "" as "CPR Number" in Product CRUD page
    #And User input "March 9 2023" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "20.3" as "Shelf Life" in Product CRUD page
    #And User input "" as "Minimum Order Quantity" in Product CRUD page
    #And User input "30" as "Safety Stock Level" in Product CRUD page
    #And User input "40" as "Forecast" in Product CRUD page
    #And User input "360" as "Supplier Lead Time" in Product CRUD page
    #And User input "99" as "Cost Per Unit" in Product CRUD page
    #And User input "199" as "Cost Per Box" in Product CRUD page
    #And User input "522" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "2.25 x 2.25 x 2.75" as "Size Small" in Product CRUD page
    #And User input "5.1 x 5.2 x 5.3" as "Size Big" in Product CRUD page
    #And User input "4.57" as "Weight Net" in Product CRUD page
    #And User input "5.21" as "Weight Gross" in Product CRUD page
    #And User input "" as "Temperature Min" in Product CRUD page
    #And User input "" as "Temperature Max" in Product CRUD page
    #And User input "40.99" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-4-5
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC5
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "prod-Team!@#$" as "Product Name" in Product CRUD page
    #And User input "prod-Team" as "Generic Name" in Product CRUD page
    #And User select "" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "" as "Product Group" in Product CRUD page
    #And User select "Orals" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu Taguig" as "Manufacturer" in Product CRUD page
    #And User input "4321-4321-abcd" as "CPR Number" in Product CRUD page
    #And User input "January 1 2023" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "" as "Shelf Life" in Product CRUD page
    #And User input "100" as "Minimum Order Quantity" in Product CRUD page
    #And User input "500" as "Safety Stock Level" in Product CRUD page
    #And User input "" as "Forecast" in Product CRUD page
    #And User input "49" as "Supplier Lead Time" in Product CRUD page
    #And User input "159" as "Cost Per Unit" in Product CRUD page
    #And User input "450" as "Cost Per Box" in Product CRUD page
    #And User input "1950" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "Box" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "4 x 4 x 2" as "Size Small" in Product CRUD page
    #And User input "3 x 10 x 10" as "Size Big" in Product CRUD page
    #And User input "10" as "Weight Net" in Product CRUD page
    #And User input "11" as "Weight Gross" in Product CRUD page
    #And User input "30" as "Temperature Min" in Product CRUD page
    #And User input "30" as "Temperature Max" in Product CRUD page
    #And User input "" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
  #
  #TS-4-6
  #Scenario: Verify if the user cannot create a product when the required columns are blank - TC6
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "prod-Team!@#$" as "Product Name" in Product CRUD page
    #And User input "" as "Generic Name" in Product CRUD page
    #And User select "Nutraceutical" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Orals" as "Product Group" in Product CRUD page
    #And User select "Oral" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu Taguig" as "Manufacturer" in Product CRUD page
    #And User input "4321-4321-abcd" as "CPR Number" in Product CRUD page
    #And User input "January 1 2023" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "12" as "Shelf Life" in Product CRUD page
    #And User input "100" as "Minimum Order Quantity" in Product CRUD page
    #And User input "500" as "Safety Stock Level" in Product CRUD page
    #And User input "300" as "Forecast" in Product CRUD page
    #And User input "1" as "Supplier Lead Time" in Product CRUD page
    #And User input "159" as "Cost Per Unit" in Product CRUD page
    #And User input "450" as "Cost Per Box" in Product CRUD page
    #And User input "1950" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "Box" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "10" as "Qty Per Pack" in Product CRUD page
    #And User input "4 x 4 x 2" as "Size Small" in Product CRUD page
    #And User input "" as "Size Big" in Product CRUD page
    #And User input "10" as "Weight Net" in Product CRUD page
    #And User input "11" as "Weight Gross" in Product CRUD page
    #And User input "30" as "Temperature Min" in Product CRUD page
    #And User input "30" as "Temperature Max" in Product CRUD page
    #And User input "10" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
    #
  #TS-5-1
  #Scenario: Verify if the user can add product even the non-required fields are blank - TC1
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page
    #Then User click "Create New Product" button in Product List page
    #And User input "Produkto-018" as "Product Name" in Product CRUD page
    #And User input "GenName018" as "Generic Name" in Product CRUD page
    #And User select "Cardiovascular" as "Product Segment" in Product CRUD page
    #And User select "" as "Product Sub-Segment" in Product CRUD page
    #And User select "Orals" as "Product Group" in Product CRUD page
    #And User select "Oral" as "Product Sub-Group" in Product CRUD page
    #And User select "VNDR#-69 - wewe" as "Supplier" in Product CRUD page
    #And User input "Manu Taguig" as "Manufacturer" in Product CRUD page
    #And User input "1235-3456-5722" as "CPR Number" in Product CRUD page
    #And User input "May 3 2022" as "CPR Expiry" in Product CRUD page
    #And User upload "" as "CPR Document" in Product CRUD page
    #And User input "24" as "Shelf Life" in Product CRUD page
    #And User input "10" as "Minimum Order Quantity" in Product CRUD page
    #And User input "" as "Safety Stock Level" in Product CRUD page
    #And User input "50" as "Forecast" in Product CRUD page
    #And User input "444" as "Supplier Lead Time" in Product CRUD page
    #And User input "22" as "Cost Per Unit" in Product CRUD page
    #And User input "52" as "Cost Per Box" in Product CRUD page
    #And User input "100" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User select "Pouch" as "Packaging" in Product CRUD page
    #And User input "temp" as "Packaging Description" in Product CRUD page
    #And User select "" as "Unit of Measure" in Product CRUD page
    #And User input "35" as "Qty Per Pack" in Product CRUD page
    #And User input "2 x 2 x 2" as "Size Small" in Product CRUD page
    #And User input "5 x 5 x 5" as "Size Big" in Product CRUD page
    #And User input "" as "Weight Net" in Product CRUD page
    #And User input "" as "Weight Gross" in Product CRUD page
    #And User input "65" as "Temperature Min" in Product CRUD page
    #And User input "65" as "Temperature Max" in Product CRUD page
    #And User input "33.3" as "Humidity" in Product CRUD page
    #And User input "" as "Storage Requirement" in Product CRUD page
    #And User upload "" as "Small Size Image" in Product CRUD page
    #And User upload "" as "Big Size Image" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then The product named "Produkto-018" is successfully created and displayed in Product List
    #And User click "Produkto-018" product name in Product List Page
    #When User click "Edit" button in Product CRUD page
    #And User verify that "Produkto-018" is the "Product Name" in Product CRUD Page
    #And User verify that "GenName018" is the "Generic Name" in Product CRUD Page
    #And User verify that "Cardiovascular" is the "Product Segment" in Product CRUD Page
    #And User verify that "" is the "Product Sub-Segment" in Product CRUD Page
    #And User verify that "Orals" is the "Product Group" in Product CRUD Page
    #And User verify that "" is the "Product Sub-Group" in Product CRUD Page
    #And User verify that "VNDR#-69 - wewe" is the "Supplier" in Product CRUD Page
    #And User verify that "Manu Taguig" is the "Manufacturer" in Product CRUD Page
    #And User verify that "1235-3456-5722" is the "CPR Number" in Product CRUD Page
    #And User verify that "May 3 2022" is the "CPR Expiry" in Product CRUD Page
    #And User verify that "24" is the "Shelf Life" in Product CRUD Page
    #And User verify that "10" is the "Minimum Order Quantity" in Product CRUD Page
    #And User verify that "" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "50" is the "Forecast" in Product CRUD Page
    #And User verify that "444" is the "Supplier Lead Time" in Product CRUD Page
    #And User verify that "22" is the "Cost Per Unit" in Product CRUD Page
    #And User verify that "52" is the "Cost Per Box" in Product CRUD Page
    #And User verify that "100" is the "Selling Price Per Box" in Product CRUD Page
    #And User click "Packaging" tab in Product CRUD page
    #And User verify that "Pouch" is the "Packaging" in Product CRUD Page
    #And User verify that "temp" is the "Packaging Description" in Product CRUD Page
    #And User verify that "" is the "Unit of Measure" in Product CRUD Page
    #And User verify that "35" is the "Qty Per Pack" in Product CRUD Page
    #And User verify that "2 x 2 x 2" is the "Size Small" in Product CRUD Page
    #And User verify that "" is the "Weight Net" in Product CRUD Page
    #And User verify that "" is the "Weight Gross" in Product CRUD Page
    #And User verify that "65" is the "Temperature Min" in Product CRUD Page
    #And User verify that "65" is the "Temperature Max" in Product CRUD Page
    #And User verify that "33.3" is the "Humidity" in Product CRUD Page
    #And User verify that "" is the "Storage Requirement" in Product CRUD Page
    #
  #TS-6-1
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC1
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Product Name" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-6-2
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC2
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Cost Per Unit" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-6-3
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC3
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Generic Name" in Product CRUD page
    #And User input "" as "CPR Number" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "" as "Humidity" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-6-4
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC4
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Product Segment" in Product CRUD page
    #And User input "" as "CPR Expiry" in Product CRUD page
    #And User input "" as "Forecast" in Product CRUD page
    #And User input "" as "Selling Price Per Box" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "" as "Size Big" in Product CRUD page
    #And User input "" as "Temperature" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-6-5
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC5
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Product Group" in Product CRUD page
    #And User input "" as "Supplier" in Product CRUD page
    #And User input "" as "Shelf Life" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "" as "Size Small" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-6-6
  #Scenario: Verify if the user cannot save the edited product when the required columns are blank - TC6
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Minimum Order Quantity" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then An error message is displayed in Product CRUD page
    #
  #TS-7-1
  #Scenario: Verify if the user can edit product even the non-required fields are blank - TC1
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Safety Stock Level" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "" as "Weight Net" in Product CRUD page
    #And User input "" as "Wegiht Gross" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then The product named "Product Automation 1MG" is successfully created and displayed in Product List
    #Validation if all details are reflected correctly
    #And User click "Product Automation 1MG" product name in Product List Page
    #When User click "Edit" button in Product CRUD page
    #And User verify that "" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "" is the "Weight Net" in Product CRUD Page
    #And User verify that "" is the "Weight Gross" in Product CRUD Page
    #
  #TS-7-2
  #Scenario: Verify if the user can edit product even the non-required fields are blank - TC2
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "" as "Safety Stock Level" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "234.24" as "Weight Net" in Product CRUD page
    #And User input "" as "Wegiht Gross" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then The product named "Product Automation 1MG" is successfully created and displayed in Product List
    #Validation if all details are reflected correctly
    #And User click "Product Automation 1MG" product name in Product List Page
    #When User click "Edit" button in Product CRUD page
    #And User verify that "" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "234.24" is the "Weight Net" in Product CRUD Page
    #And User verify that "" is the "Weight Gross" in Product CRUD Page
    #
  #TS-7-3
  #Scenario: Verify if the user can edit product even the non-required fields are blank - TC3
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #And User click "" product name in Product List Page
    #And User input "22.09" as "Safety Stock Level" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #And User input "" as "Weight Net" in Product CRUD page
    #And User input "100.32" as "Wegiht Gross" in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #Then The product named "Product Automation 1MG" is successfully created and displayed in Product List
    #Validation if all details are reflected correctly
    #And User click "Product Automation 1MG" product name in Product List Page
    #When User click "Edit" button in Product CRUD page
    #And User verify that "22.09" is the "Safety Stock Level" in Product CRUD Page
    #And User verify that "" is the "Weight Net" in Product CRUD Page
    #And User verify that "100.32" is the "Weight Gross" in Product CRUD Page
    #
  #TS-9-1
  #Scenario: Verify if the system will display an error when 2 products have the same Product Name - TC3
    #Given User login as "admin" account
    #And User click the "Master Databases" menu
    #And User click the "Product Databases" menu
    #And User click the "Product List" menu
    #When User successfully accessed the "Product List" page  
    #Then User click "Create New Product" button in Product List page
    #And User input "Citrezin 400mg1" as "Product Name" in Product CRUD page
    #And User click "Packaging" tab in Product CRUD page
    #When User click "Submit" button in Product CRUD page
    #And A duplicate product name error message is displayed
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    #
    