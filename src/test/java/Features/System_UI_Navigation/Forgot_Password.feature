@ForgotPasswordFeature
Feature: Forgot Password Feature

  Scenario Outline: Verify if the user can reset the password successfully
    Given User access the ERP Login page
    And User click the "Forgot Password" link
    And User input <email> as email address
    And User click the "Send me new password" button
    And User open a new tab in the browser
    And User access the Gmail site
    And User inputs <email> as <email site> address
    And User click the "Next" button in <email site>
    And User inputs <email password> as <email site> password
    And User click the "Next" button in <email site>
    And User click the "More" option in <email site>
    And User click the "Spam" option in <email site>
    And User open the email for reset password
    When User click the "Reset Password" link
    Then User access the reset password page
    And User change the password to <new password> password
    Then User click the "Update Password" button
    Given User access the ERP Login page
    And User input <username> as username
    And User input <new password> as password
    When User click the "Sign in" button
    Then User successfully accessed the ERP Home Page
    And User click the "User Menu" link
    When User click the "Logout" button
    Then User successfully logged out from BK ERP

    Examples:
    | email | username | email site | email password | new password |
    | cristopher.castillo@bellkenzpharma.com | LOPEZ_JV | gmail | Nshot001!@#$ | BK_0000 |
    
    
  Scenario Outline: Verify if the user cannot reset the password using unregistered email with @
    Given User access the ERP Login page
    And User click the "Forgot Password" link
    And User input <email> as email address
    When User click the "Send me new password" button
    Then An invalid email error message is displayed
    
    Examples:
    | email |
    | skoaskd@amsid.com |
    | castillo.cristopher@bellkenzpharma.com |
    
  Scenario Outline: Verify if the user cannot reset the password using unregistered email without @
    Given User access the ERP Login page
    And User click the "Forgot Password" link
    And User input <email> as email address
    When User click the "Send me new password" button
    Then No email sent dialog box displayed
    
    Examples:
    | email |
    | dlsaodkasd |
    | cristopher.castillo.bellkenzpharma.com |
    | |
    
  Scenario Outline: Verify if the user cannot reset password when new password is blank
    #Given User access the ERP Login page
    #And User click the "Forgot Password" link
    #And User input <email> as email address
    #And User click the "Send me new password" button
    #And User open a new tab in the browser
    #And User access the Gmail site
    #And User inputs <email> as <email site> address
    #And User click the "Next" button in <email site>
    #And User inputs <email password> as <email site> password
    #And User click the "Next" button in <email site>
    #And User click the "More" option in <email site>
    #And User click the "Spam" option in <email site>
    #And User open the email for reset password
    #When User click the "Reset Password" link
    #Then User access the reset password page
    #And User change the password to <new password> password
    #Then User click the "Update Password" button

    #Examples:
    #| email | username | email site | email password | new password |
    #| cristopher.castillo@bellkenzpharma.com | LOPEZ_JV | gmail | Nshot001!@#$ | BK_0000 |









    