package pageObjects.Customer_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPCustomerListPage extends BasePage {

	public ERPCustomerListPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public WebElement GetCustomerCenterHeader() {
		return waitForElementByXpathLong("//h5[contains(text(),'Customers Center')]");
	}
	
	public WebElement GetCreateNewCustomerButton() {
		return waitForElementByXpathLong("//span[@id='add_customer']");
	}
	
	//div[@data-field='customer_code']
	//div[@class='MuiDataGrid-columnHeaderWrapper scroll']

	public WebElement GetCustomerListColumnHeaders(String colHead) {
		return waitForElementByXpathLong("//div[@data-field='"+colHead+"']");
	}
	
	public WebElement GetColumnsButton() {
		return waitForElementByXpathLong("//span[contains(text(),'Columns')]");
	}
	
	public WebElement GetSearchBar() {
		return waitForElementByXpathLong("//input[@placeholder='Search…']");
	}
	
	public WebElement GetShowEntries() {
		return waitForElementByXpathLong("//*[@aria-haspopup='listbox']");
	}
	
	public WebElement GetShowEntriesList(int value) {
		return waitForElementByXpathLong("//li[@data-value='"+value+"']");
	}
	
	public WebElement GetEntriesLabel() {
		return waitForElementByXpathLong("//p[contains(text(),'of')]");
	}
	
	public WebElement GetPageControl(String page) {
		return waitForElementByXpath("//button[@title='"+page+" page']");
	}
	
	public WebElement GetCustomer(String colHead) {
		return waitForElementByXpathLong("");
	}
	
	public WebElement Get(String colHead) {
		return waitForElementByXpathLong("");
	}
	
	public WebElement GetCustomerName(String name) {
		return waitForElementByXpathLong("//a[contains(text(),'"+name+"')]");
	}
	
}
