package pageObjects.Customer_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPCustomerInformationPage extends BasePage {

	public ERPCustomerInformationPage(WebDriver driver) {
		super(driver);
	}

	public static void main(String[] args) {

	}
	
		public WebElement GetEditButton() {
				return waitForElementByXpathLong("//button[contains(text(),'Edit')]");
		}
	
	// CUSTOMER INFORMATION //////////////////////////////////////////////////
	
		public WebElement GetCustomerName() {
				return waitForElementByXpathLong("//input[@id='customer_name']");
		}
		
		public WebElement GetCustomerCode() {
			return waitForElementByXpathLong("//input[@id='customer_account_code']");
		}
		
		public WebElement GetCustomerType() {
			return waitForElementByXpathLong("//select[@id='customer_type']");
		}
		
		public WebElement GetCustomerChannel() {
			return waitForElementByXpathLong("//select[@id='customer_channel']");
		}
		
		public WebElement GetRegOrNonReg() {
			return waitForElementByXpathLong("//select[@id='regular_or_nonregular']");
		}
		
		public WebElement GetRegOrNonRegTemplate() {
			return waitForElementByXpathLong("//input[@id='si_attachment']");
		}
		
		public WebElement GetTaggedOwned() {
			return waitForElementByXpathLong("//input[@id='tagged_or_owned']");
		}
		
		public WebElement GetAO() {
			return waitForElementByXpathLong("//input[@id='AO']");
		}
		
		public WebElement GetEWT() {
			return waitForElementByXpathLong("//input[@id='ewt']");
		}
		
		public WebElement GetSCDiscount() {
			return waitForElementByXpathLong("//input[@id='sc_discount']");
		}
		
		public WebElement GetVatType() {
			return waitForElementByXpathLong("//select[@id='vat_type']");
		}
		
		public WebElement GetPOBased() {
			return waitForElementByXpathLong("//input[@id='po_based']");
		}
		
		public WebElement GetCreditLimit() {
			return waitForElementByXpathLong("//input[@id='credit_limit']");
		}
		
		public WebElement GetReturnPolicy() {
			return waitForElementByXpathLong("//select[@id='return_policy']");
		}
		
		public WebElement GetWithAgreedCharges() {
			return waitForElementByXpathLong("//input[@id='with_agreed_charges']");
		}
		
		public WebElement GetOutstandingAR() {
			return waitForElementByXpathLong("//input[@id='outstanding_ar']");
		}
		
		public WebElement GetTotalOrders() {
			return waitForElementByXpathLong("//input[@id='total_orders']");
		}
		
		public WebElement GetTinNo() {
			return waitForElementByXpathLong("//input[@id='tin_no']");
		}
		
		public WebElement GetBusinessTradeName() {
			return waitForElementByXpathLong("//input[@id='business_trade_name']");
		}
		
		public WebElement GetRegisteredName() {
			return waitForElementByXpathLong("//input[@id='registered_name']");
		}
		
		public WebElement GetBusinessType() {
			return waitForElementByXpathLong("//select[@id='business_type']");
		}

		public WebElement GetOwnerName() {
			return waitForElementByXpathLong("//input[@id='owner_name']");
		}

		public WebElement GetOwnerEmail() {
			return waitForElementByXpathLong("//input[@id='owner_email']");
		}

		public WebElement GetOwnerContactNo() {
			return waitForElementByXpathLong("//input[@id='owner_contact_no']");
		}

		public WebElement GetSaveButton() {
			return waitForElementByXpathLong("//button[contains(text(),'Save')]");
		}

		public WebElement GetActiveToggle() {
			return waitForElementByXpathLong("//input[@class='jss12 MuiSwitch-input']");
		}
		
		// BUSINESS CONTACTS //////////////////////////////////////////////////
		
		// account contact information
		public WebElement GetAccountingContactPerson() {
			return waitForElementByXpathLong("//input[@id=\"accounting_contact_person\"]");
		}
		
		public WebElement GetAccountingContactNumber() {
			return waitForElementByXpathLong("//input[@id=\"accounting_contact_no\"]");
		}
		
		public WebElement GetAccountingDesignation() {
			return waitForElementByXpathLong("//input[@id=\"accounting_designation\"]");
		}
		
		public WebElement GetAccountingEmail() {
			return waitForElementByXpathLong("//input[@id=\"accounting_email\"]");
		}
		
		// purchasing contact information
		public WebElement GetPurchasingContactPerson() {
			return waitForElementByXpathLong("//input[@id=\"purchasing_contact_person\"]");
		}
		
		public WebElement GetPurchasingContactNumber() {
			return waitForElementByXpathLong("//input[@id=\"purchasing_contact_no\"]");
		}
		
		public WebElement GetPurchasingDesignation() {
			return waitForElementByXpathLong("//input[@id=\"purchasing_designation\"]");
		}
		
		public WebElement GetPurchasingEmail() {
			return waitForElementByXpathLong("//input[@id=\"purchasing_email\"]");
		}
		
		// pharmacy contact information
		public WebElement GetPharmacyContactPerson() {
			return waitForElementByXpathLong("//input[@id=\"pharmacy_contact_person\"]");
		}
		
		public WebElement GetPharmacyContactNumber() {
			return waitForElementByXpathLong("//input[@id=\"pharmacy_contact_no\"]");
		}
		
		public WebElement GetPharmacyDesignation() {
			return waitForElementByXpathLong("//input[@id=\"pharmacy_designation\"]");
		}
		
		public WebElement GetPharmacyEmail() {
			return waitForElementByXpathLong("//input[@id=\"pharmacy_email\"]");
		}
		
		// authorized person to purchase
		public WebElement GetAuthorizedPurchasedContactPerson() {
			return waitForElementByXpathLong("//input[@id=\"authorized_purchase_person_contact_person\"]");
		}
		
		public WebElement GetAuthorizedPurchasedContactNumber() {
			return waitForElementByXpathLong("//input[@id=\"authorized_purchase_person_contact_no\"]");
		}
		
		public WebElement GetAuthorizedPurchasedDesignation() {
			return waitForElementByXpathLong("//input[@id=\"authorized_purchase_person_designation\"]");
		}
		
		public WebElement GetAuthorizedPurchasedEmail() {
			return waitForElementByXpathLong("//input[@id=\"authorized_purchase_person_email\"]");
		}
		
		// authorized person to receive
		public WebElement GetAuthorizedReceiveContactPerson() {
			return waitForElementByXpathLong("//input[@id=\"authorized_receive_person_contact_person\"]");
		}
		
		public WebElement GetAuthorizedReceiveContactNumber() {
			return waitForElementByXpathLong("//input[@id=\"authorized_receive_person_contact_no\"]");
		}
		
		public WebElement GetAuthorizedReceiveDesignation() {
			return waitForElementByXpathLong("//input[@id=\"authorized_receive_person_designation\"]");
		}
		
		public WebElement GetAuthorizedReceiveEmail() {
			return waitForElementByXpathLong("//input[@id=\"authorized_receive_person_email\"]");
		}
		
		// BILLING & SHIPPING //////////////////////////////////////////////////
		
		public WebElement GetModeofPayment() {
			return waitForElementByXpathLong("//select[@id=\"billing_mode_of_payment\"]");
		}
		
		public WebElement GetPaymentTerms() {
			return waitForElementByXpathLong("//select[@id=\"payment_terms\"]");
		}
		
		public WebElement GetInitialStocking() {
			return waitForElementByXpathLong("//select[@id=\"initial_stocking_terms\"]");
		}
		
		public WebElement GetBldgStAdd() {
			return waitForElementByXpathLong("input[@id=\"billing_bldg_street_address\"]");
		}
		
		public WebElement GetProvince() {
			return waitForElementByXpathLong("select[@name=\"billing_province\"]");
		}
		
		public WebElement GetCity() {
			return waitForElementByXpathLong("select[@name=\"billing_city\"]");
		}
		
		public WebElement GetBarangay() {
			return waitForElementByXpathLong("select[@name=\"billing_barangay\"]");
		}
		
		public WebElement GetZipCode() {
			return waitForElementByXpathLong("select[@name=\"billing_zip_code\"]");
		}
		
		public WebElement GetLBACode() {
			return waitForElementByXpathLong("input[@name=\"billing_lba_code\"]");
		}
		
		public WebElement GetLBAName() {
			return waitForElementByXpathLong("input[@name=\"billing_lba_name\"]");
		}
		
		public WebElement GetDistrict() {
			return waitForElementByXpathLong("input[@name=\"billing_district\"]");
		}
		
		public WebElement GetDeliveryScheduleCheckBox(String weekday) {
			
			int day = 0;
			
			if (weekday.equalsIgnoreCase("Monday")) {
				day = 1;
			} else if (weekday.equalsIgnoreCase("Tuesday")) {
				day = 2;
			} else if (weekday.equalsIgnoreCase("Wednesday")) {
				day = 3;
			} else if (weekday.equalsIgnoreCase("Thursday")) {
				day = 4;
			} else if (weekday.equalsIgnoreCase("Friday")) {
				day = 5;
			} else if (weekday.equalsIgnoreCase("Saturday")) {
				day = 6;
			} else if (weekday.equalsIgnoreCase("Sunday")) {
				day = 7;
			}
			
			return waitForElementByXpathLong("//input[@value=\""+day+"\" and @name=\"shipping_delivery_schedule_day\"]");
			
		}
		
		public WebElement GetDeliveryServiceProvider() {
			return waitForElementByXpathLong("//select[@id=\"shipping_delivery_service_provider\"]");
		}
		
		public WebElement GetDeliveryTime(String ind) {
				return waitForElementByXpathLong("//input[@id='shipping_delivery_time_"+ind+"']");
		}
	//	

		public WebElement GetDeleteShippingAddressButton() {
			return waitForElementByXpathLong("//button[contains(text(),'Delete Shipping Address')]");
		}

//		public WebElement Get() {
//			return waitForElementByXpathLong("//select[@name=\"shipping_addresses[0][shipping_province]\"]");
//		}
	//
//		public WebElement Get() {
//			return waitForElementByXpathLong("");
//		}
	//
//		public WebElement Get() {
//			return waitForElementByXpathLong("");
//		}
	//
//		public WebElement Get() {
//			return waitForElementByXpathLong("");
//		}
	//
	//	
	//	
	//	
//		public WebElement Get() {
//			return waitForElementByXpathLong("");
//		}

}
