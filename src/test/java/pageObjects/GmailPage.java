package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class GmailPage extends BasePage {
	
	public GmailPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetGmailTextbox() {
		return waitForElementByXpath("//input[@id='identifierId']");
	}
	
	public WebElement GetGmailPasswordTextbox() {
		return waitForElementByXpath("//input[@type='password']");
	}
	
	public WebElement GetNextButton() {
		return waitForElementByXpath("//span[@jsname='V67aGc' and contains (., \"Susunod\")]");
	}
	
	public WebElement GetMoreOption() {
		return waitForElementByXpath("//span[@class='CJ']");
	}
	
	public WebElement GetSpamOption() {
		return waitForElementByXpath("//a[@class='J-Ke n0' and contains (., 'Spam')]");
	}
	
	public WebElement GetSpamEmailTable() {
		return waitForElementByXpath("//table[@id=':mg']");
	}
	
	public WebElement GetResetEmail() {
		return waitForElementByXpath("//tr[@id=':mf']");
	}
	
	public WebElement GetResetLinkFromEmail() {
		return waitForElementByXpath("//a[@rel='noopener']");
	}
	
	
	
}
