package pageObjects.User_Role_Permission;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPUserListPage extends BasePage {
	
	public ERPUserListPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetUserControlMenu() {
		return waitForElementByXpath("//span[@class='nav-link-title' and contains (., 'User Control')]");
	}
	
	public WebElement GetUserManagementMenu() {
		return waitForElementByXpath("//a[@class='nav-link active' and contains (., 'User Management')]");
	}
	
	public WebElement GetUserListTable() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-renderingZone']");
	}
	
	public WebElement GetUserListRowTable(String rowNo) {
		return waitForElementByXpath("//div[@data-id='" + rowNo + "']");
	}
	
	public WebElement GetAddUserButton() {
		return waitForElementByXpath("//a[@id='add_user']");
	}
	
	public WebElement GetSearchTextbox() {
		return waitForElementByXpath("//input[@placeholder='Search…']");
	}
	
	public WebElement GetUserListTableHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderWrapper\"]");
	}
	
	public WebElement GetActiveHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Active\")]");
	}
	
	public WebElement GetEmployeeNoHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Employee No.\")]");
	}
	
	public WebElement GetFirstNameHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"First Name\")]");
	}
	
	public WebElement GetLastNameHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Last Name\")]");
	}
	
	public WebElement GetEmailHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Email\")]");
	}
	
	public WebElement GetUsernameHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Username\")]");
	}
	
	public WebElement GetDepartmentHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Department\")]");
	}
	
	public WebElement GetJobTitleHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Job Title\")]");
	}
	
	public WebElement GetRolesHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Roles\")]");
	}
	
	public WebElement GetActionHeader() {
		return waitForElementByXpath("//div[@class=\"MuiDataGrid-columnHeaderTitle\" and contains (., \"Action\")]");
	}
	
	public WebElement GetUsernameTextbox() {
		return waitForElementByXpath("//input[@id='username']");
	}
	
	public WebElement GetEmployeeNoTextbox() {
		return waitForElementByXpath("//input[@id='employee_no']");
	}
	
	public WebElement GetLastNameTextbox() {
		return waitForElementByXpath("//input[@id='last_name']");
	}
	
	public WebElement GetFirstNameTextbox() {
		return waitForElementByXpath("//input[@id='first_name']");
	}
	
	public WebElement GetEmailTextbox() {
		return waitForElementByXpath("//input[@id='email']");
	}
	
	public WebElement GetPasswordTextbox() {
		return waitForElementByXpath("//input[@id='password']");
	}
	
	public WebElement GetDepartmentDropdown() {
		return waitForElementByXpath("//select[@id='department_id']");
	}
	
	public WebElement GetJobDropdown() {
		return waitForElementByXpath("//select[@id='job_id']");
	}
	
	public WebElement GetContactNoTextbox() {
		return waitForElementByXpath("//input[@id='contact_no']");
	}
	
	public WebElement Get1stEditIcon() {
		return waitForElementByXpath("//a[@id='1' and @class='usersAction ']");
	}
	
	public WebElement GetCurrentPasswordTextbox() {
		return waitForElementByXpath("//input[@id='current_password']");
	}
	
	public WebElement GetNewPasswordTextbox() {
		return waitForElementByXpath("//input[@id='new_password']");
	}
	
	public WebElement GetConfirmPasswordTextbox() {
		return waitForElementByXpath("//input[@id='confirm_new_password']");
	}
	
	public WebElement GetAddButton() {
		return waitForElementByXpath("//button[@id='add']");
	}
	
	public WebElement GetEditIconPerUser(String dataID) {
		return waitForElementByXpath("//a[@class='usersAction ' and @id='Edit_" + dataID + "']");
	}
	
	public WebElement GetChangePasswordIconPerUser(String dataID) {
		return waitForElementByXpath("//a[@class='usersAction ml-2' and @id='CP_" + dataID + "']");
	}
	
	public WebElement GetEditRoleIconPerUser(String dataID) {
		return waitForElementByXpath("//a[@class='usersAction ml-2' and @id='ER_" + dataID + "']");
	}
	
	public WebElement GetSaveChangesButton() {
		return waitForElementByXpath("//button[@id='save_changes']");
	}
	
	public WebElement GetUsernameDupErrorMsg() {
		return waitForElementByXpath("//div[@id='swal2-html-container']");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
