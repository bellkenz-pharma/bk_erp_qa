package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	
	private static final int TIMEOUTlong = 50;
	private static final int TIMEOUT = 20;
	private static final int POLLING = 100;
	
	protected WebDriver driver;
	private WebDriverWait wait;
	private WebDriverWait waitLong;
	
	public BasePage(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, TIMEOUT, POLLING);
		waitLong = new WebDriverWait(driver, TIMEOUTlong, POLLING);
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, TIMEOUT), this);
	}
	
	protected WebElement waitForElementByXpath(String xpath) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.ByXPath.xpath(xpath)));
	}
	
	protected WebElement waitForElementByXpathLong(String xpath) {
		return waitLong.until(ExpectedConditions.visibilityOfElementLocated(By.ByXPath.xpath(xpath)));
	}
	
	protected WebElement waitForElementByID(String id) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.ById.id(id)));
	}
	
	protected WebElement waitForElementByIDLong(String id) {
		return wait.until(ExpectedConditions.visibilityOfElementLocated(By.ById.id(id)));
	}

}
