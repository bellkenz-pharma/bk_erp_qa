package pageObjects.Product_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPProductCRUDPage extends BasePage {
	
	public ERPProductCRUDPage (WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetProductNameTextbox() {
		return waitForElementByXpath("//input[@id='product_name']");
	}
	
	public WebElement GetGenericNameTextbox() {
		return waitForElementByXpath("//input[@id='generic_name']");
	}
	
	public WebElement GetProductSegmentDropdown() {
		return waitForElementByXpath("//select[@name='product_segment_id']");
	}
	
	public WebElement GetProductSubSegmentDropdown() {
		return waitForElementByXpath("//select[@name='product_sub_segment_id']");
	}
	
	public WebElement GetProductGroupDropdown() {
		return waitForElementByXpath("//select[@name='product_group_id']");
	}
	
	public WebElement GetProductSubGroupDropdown() {
		return waitForElementByXpath("//select[@name='product_sub_group_id']");
	}
	
	public WebElement GetSupplierDropdown() {
		return waitForElementByXpath("//select[@name='supplier_id']");
	}
	
	public WebElement GetManufacturerTextbox() {
		return waitForElementByXpath("//input[@id='manufacturer']");
	}
	
	public WebElement GetCPRNoTextbox() {
		return waitForElementByXpath("//input[@id='cpr_no']");
	}
	
	public WebElement GetCPRExpiryTextbox() {
		return waitForElementByXpath("//input[@id='cpr_expiry']");
	}
	
	public WebElement GetCPRDocumentUpload() {
		return waitForElementByXpath("//input[@id='cpr_document']");
	}
	
	public WebElement GetStockLevelTextbox() {
		return waitForElementByXpath("//input[@id='stock_level']");
	}
	
	public WebElement GetShelfLifeTextbox() {
		return waitForElementByXpath("//input[@id='shelf_life']");
	}
	
	public WebElement GetMinOrderQtyTextbox() {
		return waitForElementByXpath("//input[@id='quantity']");
	}
	
	public WebElement GetSafetyStockLevelTextbox() {
		return waitForElementByXpath("//input[@d='safety_stock_level']");
	}
	
	public WebElement GetForecastTextbox() {
		return waitForElementByXpath("//input[@id='forecast']");
	}
	
	public WebElement GetSupplierLeadTimeTextbox() {
		return waitForElementByXpath("//input[@id='supplier_lead_time']");
	}
	
	public WebElement GetCostPerUnitTextbox() {
		return waitForElementByXpath("//input[@id='cost_unit']");
	}
	
	public WebElement GetCostPerBoxTextbox() {
		return waitForElementByXpath("//input[@id='cost']");
	}
	
	public WebElement GetSellingPricePerBoxTextbox() {
		return waitForElementByXpath("//input[@id='selling_price']");
	}
	
	public WebElement GetPackagingTab() {
		return waitForElementByXpath("//a[@data-toggle='tab' and contains (., 'Packaging')]");
	}
	
	public WebElement GetProductInformationTab() {
		return waitForElementByXpath("//a[@data-toggle='tab' and contains (., 'Product Information')]");
	}
	
	public WebElement GetPackagingDropdown() {
		return waitForElementByXpath("//select[@name='packaging']");		
	}
	
	public WebElement GetPackagingDescriptionTextbox() {
		return waitForElementByXpath("//input[@id='packaging_description']");
	}
	
	public WebElement GetUnitOfMeasureDropdown() {
		return waitForElementByXpath("//select[@name='unit_measure']");
	}
	
	public WebElement GetQtyPerPackTextbox() {
		return waitForElementByXpath("//input[@id='qty_per_pack']");
	}
	
	public WebElement GetSizeSmallTextbox() {
		return waitForElementByXpath("//input[@id='small_size']");
	}
	
	public WebElement GetSizeBigTextbox() {
		return waitForElementByXpath("//input[@id='big_size']");
	}
	
	public WebElement GetWeightNetTextbox() {
		return waitForElementByXpath("//input[@id='weight_net']");
	}
	
	public WebElement GetWeightGrossTextbox() {
		return waitForElementByXpath("//input[@id='weight_gross']");
	}
	
	public WebElement GetTemperatureMinTextbox() {
		return waitForElementByXpath("//input[@id='min_temperature']");
	}
	
	public WebElement GetTemperatureMaxTextbox() {
		return waitForElementByXpath("//input[@id='max_temperature']");
	}
	
	public WebElement GetHumidityTextbox() {
		return waitForElementByXpath("//input[@id='humidity']");
	}
	
	public WebElement GetStorageRequirementTextbox() {
		return waitForElementByXpath("//input[@id='storage_requirement']");
	}
	
	public WebElement GetSmallSizeImageUpload() {
		return waitForElementByXpath("//input[@id='small_size_image_add']");
	}
	
	public WebElement GetBigSizeImageUpload() {
		return waitForElementByXpath("//input[@id='big_size_image_add']");
	}
	
	public WebElement GetSubmitButton() {
		return waitForElementByXpath("//button[@data-submit='2' and contains (., 'Submit')]");
	}
	
	public WebElement GetCRUDPageTitle() {
		return waitForElementByXpath("//h2[@id='page-title']");
	}
	
	public WebElement GetEditButton() {
		return waitForElementByXpath("//span[@class='btn btn-warning' and contains (., 'Edit')]");
	}
	
	public WebElement GetSaveChangesButton() {
		return waitForElementByXpath("//button[@type='submit' and contains (., 'Save Changes')]");
	}
	
	public WebElement GetErrorMessage() {
		return waitForElementByXpath("//h2[@id='swal2-title']");
	}
	
	public WebElement GetErrorMessageContent() {
		return waitForElementByXpath("//div[@id='swal2-html-container']");
	}
	
	public WebElement GetVATExemptCheckbox() {
		return waitForElementByXpath("//input[@id='vat_exemption']");
	}
	
	public WebElement GetOKConfirmButton() {
		return waitForElementByXpath("//button[@type='button' and contains (., 'OK')]");
	}
	
	public WebElement GetSearchCRUDPage() {
		return waitForElementByXpath("//input[@placeholder='Search here...']");
	}
	
	public WebElement GetXButton() {
		return waitForElementByXpath("//button[@type='button' and @aria-label='Close']");
	}
	
	
	
	
	
	
		
}
