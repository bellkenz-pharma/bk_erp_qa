package pageObjects.Product_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPProductListPage extends BasePage {
	
	public ERPProductListPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetProductListHeader() {
		return waitForElementByXpath("//h2[@class='page-title' and contains (., 'List of Product Stock')]");
	}
	
	public WebElement GetProductListTableHeader() {
		return waitForElementByXpath("//h2[@class='page-title']");
	}
	
	public WebElement GetCreateNewProductButton() {
		return waitForElementByXpath("//span[@class='btn btn-success btn-pill bg-header' and contains (., 'Create New Product')]");
	}
	
	public WebElement GetProductCodeColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Product Code')]");
	}
	
	public WebElement GetProductNameColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Product Name')]");
	}
	
	public WebElement GetGenericNameColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Generic Name')]");
	}
	
	public WebElement GetSupplierColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Supplier')]");
	}
	
	public WebElement GetProductSegmentColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Product Segment')]");
	}
	
	public WebElement GetProductGroupColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Product Group')]");
	}
	
	public WebElement GetPackagingColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Packaging')]");
	}
	
	public WebElement GetQtyPerPackColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Qty Per Pack')]");
	}
	
	public WebElement GetStockLevelColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Stock Level')]");
	}
	
	public WebElement GetCPRNoColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'CPR No.')]");
	}
	
	public WebElement GetCPRExpiryColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'CPR Expiry')]");
	}
	
	public WebElement GetActiveColumn() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnHeaderTitle' and contains (., 'Active')]");
	}
	
	public WebElement GetProductListHeaderElement() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-columnsContainer']");
	}
	
	public WebElement GetSearchBox() {
		return waitForElementByXpath("//input[@placeholder='Search…']");
	}
	
//	public WebElement GetProductListDataTable() {
//		return waitForElementByXpath("//div[@class='MuiDataGrid-renderingZone']");
//	}
	
	public WebElement GetProductListDataTable() {
		return waitForElementByXpath("//div[@class='MuiDataGrid-virtualScrollerRenderZone css-1inm7gi']");
	}
	
	public WebElement GetProductCRUDDataTable() {
		return waitForElementByXpath("//div[@class='list-group']");
	}
	
	public WebElement GetProductCodeLink(String prodCode) {
		return waitForElementByXpath("//a[@href='/view/products/list/" + prodCode +"']");
	}
	
	
	
	
	

}
