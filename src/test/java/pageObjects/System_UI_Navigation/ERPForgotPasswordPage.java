package pageObjects.System_UI_Navigation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPForgotPasswordPage extends BasePage {
	
	public ERPForgotPasswordPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetEmailResetTextbox() {
		return waitForElementByXpath("//input[@id='email']");
	}
	
	public WebElement GetSendMeButton() {
		return waitForElementByXpath("//button[@value='Submit']");
	}
	
	public WebElement GetNewPasswordTextBox() {
		return waitForElementByXpath("//input[@name='password']");
	}
	
	public WebElement GetConfirmPasswordTextBox() {
		return waitForElementByXpath("//input[@name='password2']");
	}
	
	public WebElement GetUpdatePasswordButton() {
		return waitForElementByXpath("//button[@type='submit']");
	}
	
	public WebElement GetEmailSentDialogBox() {
		return waitForElementByXpath("//h2[@id='swal2-title']");
	}
	
	public WebElement GetPasswordChangedDialogBox() {
		return waitForElementByXpath("//h2[@id='swal2-title']");
	}
	
	public WebElement GetPasswordChangedOKButton() {
		return waitForElementByXpath("//button[@type='button' and contains (., \"OK\")]");
	}
	
	public WebElement GetInvalidEmailDialogBox() {
		return waitForElementByXpath("//h2[@id='swal2-title' and contains (., \"Invalid Email\")]");
	}
	
	
	
	
	
	
	
	
	

}
