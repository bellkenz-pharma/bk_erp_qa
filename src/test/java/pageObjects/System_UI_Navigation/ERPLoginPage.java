package pageObjects.System_UI_Navigation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPLoginPage extends BasePage {
	
	public ERPLoginPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetUsernameTextbox() {
		return waitForElementByXpathLong("//input[@id='username']");
	}
	
	public WebElement GetPasswordTextbox() {
		return waitForElementByXpathLong("//input[@id='password']");
	}
	
	public WebElement GetSignInButton() {
		return waitForElementByXpathLong("//button[@type='submit' and contains (., \"Sign in\")]");
	}
	
	public WebElement GetErrorLoginErrorMessage() {
		return waitForElementByXpathLong("//div[@id='swal2-html-container']");
	}
	
	public WebElement GetForgotPasswordLink() {
		return waitForElementByXpathLong("//a[@href='/provide-email' and contains (., \"Forgot password\")]");
	}
	
	
}
