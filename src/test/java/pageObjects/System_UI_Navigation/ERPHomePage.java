package pageObjects.System_UI_Navigation;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPHomePage extends BasePage {
	
	public ERPHomePage(WebDriver driver) {
		super (driver);
	}
	
	public WebElement GetBKLogo() {
		return waitForElementByXpathLong("//img[@src='/Spread Version.png']");
	}
	
	public WebElement GetUserMenu() {
		return waitForElementByXpathLong("//a[@aria-label='Open user menu']");
	}
	
	// MASTER DATABASE
	public WebElement GetMasterDatabasesMenu() {
		return waitForElementByXpath("//span[@class='nav-link-title' and contains (., 'Master Databases')]");
//		return waitForElementByXpath("//a[contains(text(),'Master Databases')]");
	}
	// PRODUCT DATABASE
	public WebElement GetProductDatabasesSubMenu() {
		return waitForElementByXpath("//a[@class='dropdown-item dropdown-toggle nav-item-child' and contains (., 'Product Databases')]");
	}
		
	public WebElement GetProductListSubMenu() {
		return waitForElementByXpath("//a[@class='dropdown-item' and contains (., 'Product List')]");
	}
	
	// CUSTOMER DATABASE
	public WebElement GetCustomerDatabasesSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Customer Databases')]");
	}
	
	public WebElement GetCustomerListSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Customer List')]");
	}
	
	public WebElement GetSMPPProductMaintenanceSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'SMPP Product Maintenance')]");
	}
	
	// COURIER DATABASE
	public WebElement GetCourierDatabasesSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Courier Databases')]");
	}
	
	public WebElement GetCourierListSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Courier List')]");
	}
	
	public WebElement GetCourierLocationSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Courier Location')]");
	}
	
	public WebElement GetCustomerCourierSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Customer Courier')]");
	}
	
	// SUB LEDGER DATABASE
	public WebElement GetSubLedgerDatabaseSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Sub Ledger Databases')]");
	}
	
	public WebElement GetSalesSpecialJournalSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Sales Special Journal')]");
	}
	
	public WebElement GetReceivableSubLedgerSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Receivable Sub Ledger')]");
	}
	
	public WebElement GetInventorySubLedgerSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Sub Ledger')]");
	}
	
	// WAREHOUSE DATABASE
	public WebElement GetWarehouseDatabaseSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Warehouse Database')]");
	}
	
	public WebElement GetWarehouseListSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Warehouse List')]");
	}
	
	public WebElement GetZoneAssignmentSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Zone Assignment')]");
	}
	
	// USER DATABASE
	public WebElement GetUserDatabaseSubMenu() {
		return waitForElementByXpath("//a[@href='#User Database' and contains (., 'User Database')]");
	}
	
	public WebElement GetUsersControlSubMenu() {
		return waitForElementByXpath("//a[@href='/user-control' and contains (., 'Users Control')]");
	}
	
	public WebElement GetLogoutButton() {
		return waitForElementByXpathLong("//a[@class='dropdown-item']");
	}
	
	// INVENTORY
	public WebElement GetInventorySubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory')]");
	}

	public WebElement GetInventoryReceiptSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Receipt')]");
	}

	public WebElement GetInventoryReceiptInspectionSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory REceipt Inspection')]");
	}

	public WebElement GetInventoryReceiptApprovalSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Receipt Approval')]");
	}

	public WebElement GetInventoryAdjustmentSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Adjustment')]");
	}

	public WebElement GetInventoryReportSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Report')]");
	}

	public WebElement GetInventoryTransferSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Transfer')]");
	}

	public WebElement GetInventoryTransferApprovalSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Inventory Transfer Approval')]");
	}

	public WebElement GetWarehouseSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Warehouse')]");
	}
	
	public WebElement GetWarehousePalletAssignmentSubMenu() {
		return waitForElementByXpath("//a[contains(text(),'Warehouse & Pallet Assignment')]");
	}

	
}
