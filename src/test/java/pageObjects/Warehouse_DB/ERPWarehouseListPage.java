package pageObjects.Warehouse_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPWarehouseListPage extends BasePage {
	
	public ERPWarehouseListPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetWarehouseHeader() {
		return waitForElementByXpath("//h2[contains(text(),'Warehouse Center')]");
	}
	
	public WebElement GetCreateNewWarehouseButton() {
		return waitForElementByXpath("//a[contains(text(),'Create New Warehouse')]");
	}
	
	

}
