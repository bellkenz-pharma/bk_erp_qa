package pageObjects.Warehouse_DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import pageObjects.BasePage;

public class ERPWarehouseCRUDPage extends BasePage {
	
	public ERPWarehouseCRUDPage(WebDriver driver) {
		super(driver);
	}
	
	public WebElement GetWarehouseNameTextbox() {
		return waitForElementByXpath("//input[@name='warehouse_name']");
	}
	
	public WebElement GetBuildingTextbox() {
		return waitForElementByXpath("//input[@name='building']");
	}
	
	public WebElement GetLotStreetTextbox() {
		return waitForElementByXpath("//input[@name='lot_street']");
	}
	
	public WebElement GetFloorTextbox(int floorIndex) {
		return waitForElementByXpath("//input[@id='" + floorIndex + "' and @name='floors_dummy[]']");
	}
	
	public WebElement GetCountryDropdown() {
		return waitForElementByXpath("//select[@name='country']");
	}
	
	public WebElement GetRegionDropdown() {
		return waitForElementByXpath("//select[@name='region']");
	}
	
	public WebElement GetProvinceDropdown() {
		return waitForElementByXpath("//select[@name='province']");
	}
	
	public WebElement GetCityDropdown() {
		return waitForElementByXpath("//select[@name='city']");
	}
	
	public WebElement GetBarangayDropdown() {
		return waitForElementByXpath("//select[@name='barangay']");
	}
	
	public WebElement GetZipDropdown() {
		return waitForElementByXpath("//select[@name='zip']");
	}
	
	public WebElement GetRowTextbox(int rowIndex) {
		return waitForElementByXpath("//input[@id='" + rowIndex + "' and @name='row_pallet[]']");
	}
	
	public WebElement GetColumnTextbox(int columnIndex) {
		return waitForElementByXpath("//input[@id='" + columnIndex + "' and @name='row_pallet[]']");
	}
	
	public WebElement GetShelfTextbox(int shelfIndex) {
		return waitForElementByXpath("//input[@id='" + shelfIndex + "' and @name='shelf_pallet[]']");
	}
	
	public WebElement GetFloorImageButton(int floorImageIndex) {
		return waitForElementByXpath("//input[@id='" + floorImageIndex + "' and @data-name='image_reference']");
	}

}
