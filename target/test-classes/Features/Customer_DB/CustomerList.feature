@CustomerList
Feature: Customer List Feature

  @CustomerListView
  Scenario: Verify if user is able to view customer list page
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Customer Databases" menu
    And User click the "Customer List" menu
    When User successfully accessed the Customer List page
    Then The "Create New Customer" button is displayed in Customer List page
    And The "Customer Code" table column is displayed in Customer List page
    And The "Customer Name" table column is displayed in Customer List page
    And The "Tin" table column is displayed in Customer List page
    And The "Amount Outstanding" table column is displayed in Customer List page
    And The "Terms" table column is displayed in Customer List page
    And The "Channel" table column is displayed in Customer List page
    And The "Sales Manager" table column is displayed in Customer List page
    And The "Sales Officer" table column is displayed in Customer List page
    And The "SAR" table column is displayed in Customer List page
    And The "HSAR" table column is displayed in Customer List page
    And The "Status" table column is displayed in Customer List page
    And The "EWT" table column is displayed in Customer List page
    And The "SC Discount" table column is displayed in Customer List page
    Then The "Columns" button is displayed in Customer List page
    Then The "Search" button is displayed in Customer List page

  #And Show Number of Entries dropdown is displayed in Customer List Page
  @CustomerListSort
  Scenario: Verify if user is able to sort and unsort each column
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Customer Databases" menu
    And User click the "Customer List" menu
    When User successfully accessed the Customer List page
    And User sort "Customer Code" column
    And User sort "Customer Name" column
    And User sort "Tin" column
    And User sort "Amount Outstanding" column
    And User sort "Terms" column
    And User sort "Channel" column
    And User sort "Sales Manager" column
    And User sort "Sales Officer" column
    And User sort "SAR" column
    And User sort "HSAR" column
    And User sort "Status" column
    And User sort "EWT" column
    And User sort "SC Discount" column

  @CustomerListPagination
  Scenario: Verify if Customer Table pagination options
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "Customer Databases" menu
    And User click the "Customer List" menu
    When User successfully accessed the Customer List page
    Then User show "25" entries in Customer List page
    And User view "Next" Page in Customer List page
    And User view "Previous" Page in Customer List page
    Then User show "50" entries in Customer List page
    Then User show "75" entries in Customer List page
    Then User show "100" entries in Customer List page
