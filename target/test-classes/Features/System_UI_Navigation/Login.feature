@LoginFeature
Feature: Login Feature

  Scenario Outline: Valid Credentials
    Given User access the ERP QA Server Login page
    And User input <username> as username
    And User input <password> as password
    When User click the "Sign in" button
    Then User successfully accessed the ERP Home Page
    And User click the "User Menu" link
    When User click the "Logout" button
    Then User successfully logged out from BK ERP
    
    Examples:
    | username | password |
    | LOPEZ_JV | BK_2248 |
    | DESUYO_AH | BK_3149 |
    
    
  Scenario Outline: Invalid Credentials
    Given User access the ERP QA Server Login page
    And User input <username> as username
    And User input <password> as password
    When User click the "Sign in" button
    Then An "Invalid Credentials" error message displayed
    
    Examples:
    | username | password |
    | | |
    | BAUTISTA_AP_LG | |
    | | BK_3149 |
    #| TOPE_C | 1A2B3C |
    #| JOSHUA_DJ | QATEAM1 |
    