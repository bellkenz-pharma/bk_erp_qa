@UserListCRUDFeature
Feature: User List CRUD Feature

  Scenario Outline: Verify if the admin can ADD USER
    Given User login as "admin" account
    And User click the "Master Databases" menu
    And User click the "User Database" menu
    And User click the "Users Control" menu
    And User click "User Management" tab
    #When User click the "Add User" button
    #Then User input <username> as "username" in Add User
    #And User input <employee no> as "employee number" in Add User
    #And User input <last name> as "last name" in Add User
    #And User input <first name> as "first name" in Add User
    #And User input <email> as "email" in Add User
    #And User input <password> as "password" in Add User
    #And User input <department> as "department" in Add User
    #And User input <job> as "job" in Add User
    #And User input <contact no> as "contact number" in Add User
    #When User click the "Add" button in Add User
    Then The employee number <employee no> is now added successfully
    
    Examples:
    | username | employee no | last name | first name | email | password | department | job | contact no |
    | JUAN-DC | TEMP-001 | Dela Cruz | Juan | juan.delacruz@bellkenzpharma.com | Password1234 | Accounting | Manager | 092938428 |
    #| CASTILLO-MC | TEMP2020 | Castillo | Maria | maria.castillo@bellkenzpharma.com | BKERP2021 | Warehouse | Manager | 09443842813 |
    
  #Scenario Outline: Verify if the admin can EDIT USER
    #Given User login as "Tope" account
    #And User click the "User Control" menu
    #And User click "User Management" tab
    #When User click the "Edit" icon of <employee no> user
    #Then User input <username> as "username" in Edit User
    #And User input <employee no> as "employee number" in Edit User
    #And User input <last name> as "last name" in Edit User
    #And User input <first name> as "first name" in Edit User
    #And User input <email> as "email" in Edit User
    #And User input <password> as "password" in Edit User
    #And User input <department> as "department" in Edit User
    #And User input <job> as "job" in Edit User
    #And User input <contact no> as "contact number" in Edit User
    #When User click the "Save Changes" button in Edit User
    #Then The employee number <employee no> is now added successfully
    #
    #Examples:
    #| username | employee no | last name | first name | email | password | department | job | contact no |
    #| JUAN-DC | TEMP-001 | Luna | Juan | juan.delacruz@bellkenzpharma.net | Password1234 | IT | Manager | 092938428 |
    #| 2020TEMP | TEMP2020 | Lucero | Pia | maria.castillo@bellkenzpharma.com | BKERP2021 | Warehouse | Developer | 0911111111 |
    
  #Scenario Outline: Verify if the admin can CHANGE PASSWORD
    #Given User login as "Tope" account
    #And User click the "User Control" menu
    #And User click "User Management" tab
    #When User click the "Change Password" icon of <employee no> user
    #Then User input the <current pass> as "current" password
    #And User input the <new pass> as "new" password
    #When User click the "Update Password" button
    #Then User <employee no> is successfully changed the password
    #And User click the "User Menu" link
    #When User click the "Logout" button
    #Then User login the <new pass> of <employee no> with <username> username
    #And User successfully login using the new password
    #
    #
    #Examples:
    #| username | employee no | current pass | new pass |
    #| JUAN-DC | TEMP-001 | Password1234 | 1234password |
    #| 2020TEMP | TEMP2020 | BKERP2021 | BK2021PI |
    
  #Scenario Outline: Verify if the user cannot add user when duplicate username
    #Given User login as "Tope" account
    #And User click the "User Control" menu
    #And User click "User Management" tab
    #When User click the "Add User" button
    #Then User input <username> as "username" in Add User
    #And User input <employee no> as "employee number" in Add User
    #And User input <last name> as "last name" in Add User
    #And User input <first name> as "first name" in Add User
    #And User input <email> as "email" in Add User
    #And User input <password> as "password" in Add User
    #And User input <department> as "department" in Add User
    #And User input <job> as "job" in Add User
    #And User input <contact no> as "contact number" in Add User
    #When User click the "Add" button in Add User
    #Then A duplicate "Username" error message displayed
    #
    #Examples:
    #| username | employee no | last name | first name | email | password | department | job | contact no |
    #| JUAN-DC | TEMP-001 | Luna | Juan | juan.delacruz@bellkenzpharma.net | Password1234 | IT | Manager | 092938428 |
    #| 2020TEMP | TEMP2020 | Lucero | Pia | maria.castillo@bellkenzpharma.com | BKERP2021 | Warehouse | Developer | 0911111111 |
    #
  #Scenario Outline: Verify if the user cannot add user when duplicate employee no
    #Given User login as "Tope" account
    #And User click the "User Control" menu
    #And User click "User Management" tab
    #When User click the "Add User" button
    #Then User input <username> as "username" in Add User
    #And User input <employee no> as "employee number" in Add User
    #And User input <last name> as "last name" in Add User
    #And User input <first name> as "first name" in Add User
    #And User input <email> as "email" in Add User
    #And User input <password> as "password" in Add User
    #And User input <department> as "department" in Add User
    #And User input <job> as "job" in Add User
    #And User input <contact no> as "contact number" in Add User
    #When User click the "Add" button in Add User
    #Then A duplicate "Employee No" error message displayed
    #
    #Examples:
    #| username | employee no | last name | first name | email | password | department | job | contact no |
    #| 1010j | TEMP-001 | Luna | Juan | juan.delacruz@bellkenzpharma.net | Password1234 | IT | Manager | 092938428 |
    #| t2e0m2p1t | TEMP2020 | Lucero | Pia | maria.castillo@bellkenzpharma.com | BKERP2021 | Warehouse | Developer | 0911111111 |
    #
  #Scenario Outline: Verify if the user cannot add user when duplicate email
    #Given User login as "Tope" account
    #And User click the "User Control" menu
    #And User click "User Management" tab
    #When User click the "Add User" button
    #Then User input <username> as "username" in Add User
    #And User input <employee no> as "employee number" in Add User
    #And User input <last name> as "last name" in Add User
    #And User input <first name> as "first name" in Add User
    #And User input <email> as "email" in Add User
    #And User input <password> as "password" in Add User
    #And User input <department> as "department" in Add User
    #And User input <job> as "job" in Add User
    #And User input <contact no> as "contact number" in Add User
    #When User click the "Add" button in Add User
    #Then A duplicate "Email" error message displayed
    #
    #Examples:
    #| username | employee no | last name | first name | email | password | department | job | contact no |
    #| 1010j | L1l1l1 | Luna | Juan | juan.delacruz@bellkenzpharma.net | Password1234 | IT | Manager | 092938428 |
    #| t2e0m2p1t | BKBK1122 | Lucero | Pia | maria.castillo@bellkenzpharma.com | BKERP2021 | Warehouse | Developer | 0911111111 |
    #
    #
    #
    #
    