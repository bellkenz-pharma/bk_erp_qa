$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/java/Features/Customer_DB/CustomerInformation.feature");
formatter.feature({
  "name": "Customer Information feature",
  "description": "",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@CustomerInformation"
    }
  ]
});
formatter.scenario({
  "name": "Verify if user is able to view Customer Information",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@CustomerInformation"
    },
    {
      "name": "@CustomerinformationView"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "User login as \"admin\" account",
  "keyword": "Given "
});
formatter.match({
  "location": "Login_Steps.user_login_as_something_account(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User click the \"Master Databases\" menu",
  "keyword": "And "
});
formatter.match({
  "location": "Commons.user_click_the_something_menu(String)"
});
formatter.result({
  "error_message": "org.openqa.selenium.TimeoutException: Expected condition failed: waiting for visibility of element located by By.xpath: //span[@class\u003d\u0027nav-link-title\u0027 and contains (., \u0027Master Databases\u0027)] (tried for 20 second(s) with 100 milliseconds interval)\r\n\tat org.openqa.selenium.support.ui.WebDriverWait.timeoutException(WebDriverWait.java:113)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:283)\r\n\tat pageObjects.BasePage.waitForElementByXpath(BasePage.java:29)\r\n\tat pageObjects.System_UI_Navigation.ERPHomePage.GetMasterDatabasesMenu(ERPHomePage.java:24)\r\n\tat stepDefinitions.Common.Commons.user_click_the_something_menu(Commons.java:13)\r\n\tat ✽.User click the \"Master Databases\" menu(file:src/test/java/Features/Customer_DB/CustomerInformation.feature:25)\r\nCaused by: org.openqa.selenium.NoSuchElementException: Cannot locate an element using By.xpath: //span[@class\u003d\u0027nav-link-title\u0027 and contains (., \u0027Master Databases\u0027)]\nFor documentation on this error, please visit: http://seleniumhq.org/exceptions/no_such_element.html\nBuild info: version: \u00273.14.0\u0027, revision: \u0027aacccce0\u0027, time: \u00272018-08-02T20:19:58.91Z\u0027\nSystem info: host: \u0027LENOVOL14-04\u0027, ip: \u0027192.168.1.100\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002717.0.2\u0027\nDriver info: driver.version: unknown\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.lambda$findElement$0(ExpectedConditions.java:896)\r\n\tat java.base/java.util.Optional.orElseThrow(Optional.java:403)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.findElement(ExpectedConditions.java:895)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions.access$000(ExpectedConditions.java:44)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:206)\r\n\tat org.openqa.selenium.support.ui.ExpectedConditions$7.apply(ExpectedConditions.java:202)\r\n\tat org.openqa.selenium.support.ui.FluentWait.until(FluentWait.java:260)\r\n\tat pageObjects.BasePage.waitForElementByXpath(BasePage.java:29)\r\n\tat pageObjects.System_UI_Navigation.ERPHomePage.GetMasterDatabasesMenu(ERPHomePage.java:24)\r\n\tat stepDefinitions.Common.Commons.user_click_the_something_menu(Commons.java:13)\r\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke0(Native Method)\r\n\tat java.base/jdk.internal.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:77)\r\n\tat java.base/jdk.internal.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)\r\n\tat java.base/java.lang.reflect.Method.invoke(Method.java:568)\r\n\tat cucumber.runtime.Utils$1.call(Utils.java:26)\r\n\tat cucumber.runtime.Timeout.timeout(Timeout.java:16)\r\n\tat cucumber.runtime.Utils.invoke(Utils.java:20)\r\n\tat cucumber.runtime.java.JavaStepDefinition.execute(JavaStepDefinition.java:57)\r\n\tat cucumber.runner.PickleStepDefinitionMatch.runStep(PickleStepDefinitionMatch.java:50)\r\n\tat cucumber.runner.TestStep.executeStep(TestStep.java:65)\r\n\tat cucumber.runner.TestStep.run(TestStep.java:50)\r\n\tat cucumber.runner.PickleStepTestStep.run(PickleStepTestStep.java:43)\r\n\tat cucumber.runner.TestCase.run(TestCase.java:46)\r\n\tat cucumber.runner.Runner.runPickle(Runner.java:49)\r\n\tat cucumber.runtime.junit.PickleRunners$NoStepDescriptions.run(PickleRunners.java:146)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:68)\r\n\tat cucumber.runtime.junit.FeatureRunner.runChild(FeatureRunner.java:23)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat cucumber.runtime.junit.FeatureRunner.run(FeatureRunner.java:73)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:123)\r\n\tat cucumber.api.junit.Cucumber.runChild(Cucumber.java:65)\r\n\tat org.junit.runners.ParentRunner$3.run(ParentRunner.java:290)\r\n\tat org.junit.runners.ParentRunner$1.schedule(ParentRunner.java:71)\r\n\tat org.junit.runners.ParentRunner.runChildren(ParentRunner.java:288)\r\n\tat org.junit.runners.ParentRunner.access$000(ParentRunner.java:58)\r\n\tat org.junit.runners.ParentRunner$2.evaluate(ParentRunner.java:268)\r\n\tat cucumber.api.junit.Cucumber$RunCucumber.evaluate(Cucumber.java:147)\r\n\tat org.junit.runners.ParentRunner.run(ParentRunner.java:363)\r\n\tat org.eclipse.jdt.internal.junit4.runner.JUnit4TestReference.run(JUnit4TestReference.java:93)\r\n\tat org.eclipse.jdt.internal.junit.runner.TestExecution.run(TestExecution.java:40)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:529)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.runTests(RemoteTestRunner.java:756)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.run(RemoteTestRunner.java:452)\r\n\tat org.eclipse.jdt.internal.junit.runner.RemoteTestRunner.main(RemoteTestRunner.java:210)\r\n",
  "status": "failed"
});
formatter.step({
  "name": "User click the \"Customer Databases\" menu",
  "keyword": "And "
});
formatter.match({
  "location": "Commons.user_click_the_something_menu(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click the \"Customer List\" menu",
  "keyword": "And "
});
formatter.match({
  "location": "Commons.user_click_the_something_menu(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User successfully accessed the Customer List page",
  "keyword": "When "
});
formatter.match({
  "location": "CustomerListPage.user_successfully_accessed_the_customer_list_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User click \"Customer A\" link in Customer List page",
  "keyword": "Then "
});
formatter.match({
  "location": "CustomerInformationPage.user_click_something_link_in_customer_list_page(String)"
});
formatter.result({
  "status": "skipped"
});
formatter.step({
  "name": "User verify information displayed in Customer Info page",
  "keyword": "And "
});
formatter.match({
  "location": "CustomerInformationPage.user_verify_information_displayed_in_customer_info_page()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "status": "passed"
});
});